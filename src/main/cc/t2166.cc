
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=ll; /* type of values */
using F=ll; /* type of upates */

S st_op(S a, S b) {
    return max(a,b);
}

const ll INF=1e18;
S st_e() {
    return -INF;
}

/* This applies an update to some value.
 * ie if st_op is mul, then we would 
 * return f*x.
 */
S st_mapping(F f, S x) {
    return f+x;
}

/* This creates a mapping from two given mappings.
 * ie if st_op is mul, then we would
 * return f*g, since f*(g*x)==(f*g)*x
 */
F st_composition(F f, F g) {
    return f+g;
}

/* this is the neutral update. ie
 * if st_op is + then st_id would be 0,
 * if st_op is mul then st_id would be 1,
 * if st_op is min then st_id is INF
 * It is similar to st_e() in some sense.
 */
F st_id() {
    return 0LL;
}

using segtree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

void solve() {
    cini(n);
    cini(q);
    vector<ll> a(n);
    for(int i=0; i<n; i++) {
        cin>>a[i];
        if(i>0)
            a[i]+=a[i-1];
    }

    segtree seg(a);
    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            int k;
            ll u;
            cin>>k>>u;
            k--;
            ll oV1=seg.get(k);
            ll oV2=0;
            if(k>0)
                oV2=seg.get(k-1);

            //cerr<<"oldVal="<<oldVal<<" update="<<u-oldVal<<endl;
            ll diff=u-(oV1-oV2);
            seg.apply(k, n, diff);
        } else if(t==2) {
            cini(l); l--;
            cini(r); 
            ll pre=0;
            if(l>0)
                pre=seg.get(l-1);

            //cerr<<"l="<<l<<" r="<<r<<" pre="<<pre<<endl;
            ll ans=seg.prod(l,r)-pre;
            cout<<max(0LL,ans)<<endl;
        } else
            assert(false);
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
