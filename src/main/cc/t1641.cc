/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, x;
    cin>>n>>x;
    vector<pair<int, int>> a(n);
    fori(n) {
        cin>>a[i].first;
        a[i].second=i;
    }
    sort(a.begin(), a.end());

    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            int s=x-a[i].first-a[j].first;
            if(s>0) {
                auto it=lower_bound(a.begin()+j+1, a.end(), make_pair(s, -1));
                if(it!=a.end() && it->first==s) {
                    int idx=distance(a.begin(), it);
                    cout<<a[i].second+1<<" "<<a[j].second+1<<" "<<a[idx].second+1<<endl;
                    return 0;
                }
            }
        }
    }

    cout<<"IMPOSSIBLE"<<endl;

}

