/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int INF=4e18;

/* Two possible solutions:
 * 1. Dijkstra where the state includes if with or without having used the coupon.
 * 2. Dijkstra without using the coupon, once from start, once reversed from end.
 *    Then iterate all edges, and assume it to be the coupon-edge.
 */
void solve() {
    cini(n);
    cini(m);

    vector<vector<pii>> adj(n);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        cini(w);
        adj[a].emplace_back(b,w);
    }

    vvi dp(2, vi(n, INF)); /* dp[0] without using coupon, dp[1] with using coupon */
    dp[0][0]=0;
    using t3=tuple<int,int,int>;
    priority_queue<t3> q;
    q.emplace(0,0,0);   /* dpval,v,cused */
    while(q.size()) {
        auto [dpval,v,cused]=q.top();
        q.pop();
        if(dp[cused][v]!=-dpval)
            continue;

        for(auto [chl,w] : adj[v]) {
            if(cused) {
                if(dp[1][chl]>dp[1][v]+w) {
                    dp[1][chl]=dp[1][v]+w;
                    q.emplace(-dp[1][chl], chl, 1);
                }
            } else {
                if(dp[0][chl]>dp[0][v]+w) {
                    dp[0][chl]=dp[0][v]+w;
                    q.emplace(-dp[0][chl], chl, 0);
                }
                if(dp[1][chl]>dp[0][v]+w/2) {
                    dp[1][chl]=dp[0][v]+w/2;
                    q.emplace(-dp[1][chl], chl, 1);
                }
            }
        }
    }
    cout<<min(dp[1][n-1], dp[0][n-1])<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
