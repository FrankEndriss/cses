
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/** stolen from tourist, Problem D, https://codeforces.com/contest/1208/standings/page/1 */
template <typename T>
struct fenwick {
    vector<T> fenw;
    int n;

    fenwick(int _n) : n(_n) {
        fenw.resize(n);
    }

    /* update add */
    void update(int x, T v) {
        while (x < n) {
            fenw[x] += v;
            x |= (x + 1);
        }
    }

    /* get sum of range (0,x), including x */
    T query(int x) {
        T v{};
        while (x >= 0) {
            v += fenw[x];
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

void solve() {
    cini(n);
    vector<tuple<int,int,int>> a(n);

    map<int,int> mY;

    for(int i=0;i<n; i++) {
        cini(x);
        cini(y);
        mY[y]=y;
        a[i]={x,y,i};
    }

    /* compress y */
    int idY=0;
    for(auto it=mY.begin(); it!=mY.end(); it++) 
        it->second=idY++;

    fenwick<int> upper(idY);    /* set of y with bgeq x */
    fenwick<int> lower(idY);    /* set of y with leq x */

    for(int i=0; i<n; i++) {
        auto [x,y,j]=a[i];
        y=mY[y];
        assert(y<idY);
        upper.update(y, 1);
        a[i]={x,-y,j};
    }

    sort(all(a));

    /* ans[i].first=cnt of ranges contained in range i */
    vector<pii> ans(n, {-1,-1});

    for(int i=0; i<n; i++) {
        const int idx=get<2>(a[i]);
        assert(idx<n);
        const int y=-get<1>(a[i]);
        assert(y<idY);

        upper.update(y,-1);
        /* ranges in upper have bgeq x, so the ones among them
         * with leq y are contained in this range */
        ans[idx].first=upper.query(y);

        /* ranges in lower have leq x, so the ones having bgeq y
         * contain this range.
         */
        ans[idx].second=lower.query(idY-1)-lower.query(y-1);
        lower.update(y,1);
    }

    for(int i=0; i<n; i++)
        cout<<(ans[i].first>0)<<" ";
    cout<<endl;
    for(int i=0; i<n; i++)
        cout<<(ans[i].second>0)<<" ";
    cout<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
