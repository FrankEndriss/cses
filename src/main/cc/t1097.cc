/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<ll> x(n);
    fori(n)
        cin>>x[i];

    vector<vector<pair<ll, ll>>> dp(n, vector<pair<ll, ll>>(n));
    /* dp[i][j].first = max score of first if number from i to j are available. */
    /* dp[i][j].second = max score of second if number from i to j are available. */

    for(int i=0; i<n; i++) {
        dp[i][i].first=x[i];
        dp[i][i].second=0;
    }
    for(int i=0; i<n-1; i++) {
        dp[i][i+1].first=max(x[i], x[i+1]);
        dp[i][i+1].second=min(x[i], x[i+1]);
    }

    for(int j=2; j<n; j++) {
        for(int i=0; i+j<n; i++) {
            if(dp[i+1][i+j].second+x[i] > dp[i][i+j-1].second+x[i+j]) {
                dp[i][i+j].first=dp[i+1][i+j].second+x[i];
                dp[i][i+j].second=dp[i+1][i+j].first;
            } else {
                dp[i][i+j].first=dp[i][i+j-1].second+x[i+j];
                dp[i][i+j].second=dp[i][i+j-1].first;
            }
        }
    }

#ifdef DEBUG
for(int i=0; i<n; i++) {
for(int j=0; j<n; j++)
    cout<<dp[i][j].first<<"/"<<dp[i][j].second<<" ";
cout<<endl;
}
#endif

    cout<<dp[0][n-1].first<<endl;
}

