
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/*
 * dp[i][j]=number of towers with hight i and 
 *   j=0: top row single elemnt; 
 *   j=1: top row two elements
 *
 * dp[1][0]=1;
 * dp[1][1]=1;
 * dp[i][0]=1+dp[i-1][0]+dp[i-1][1];
 * dp[i][1]=0+dp[i-1][0]+dp[i-1][1];
 * ...
 * Most likely we can somehow put this into a matrix
 * and use matrix potentiation instead of 
 * running O(logN)
 */
const int N=1e6+1;
unsigned int dp[N][2];
//vvi ans(N, vi(2));
const int MOD=1e9+7;

inline unsigned int pl(const unsigned int a, const unsigned int b) {
    unsigned int ans=a+b;
    if(ans>=MOD)
        ans-=MOD;
    return ans;
}

void init() {
    dp[1][0]=1;
    dp[1][1]=1;
    for(int i=2; i<N; i++) {
        dp[i][0]=pl(dp[i-1][0], pl(dp[i-1][0],dp[i-1][1]));
        const unsigned int aux=pl(dp[i-1][1],dp[i-1][1]);
        dp[i][1]=pl(dp[i-1][0], pl(aux,aux));
    }
}

void solve() {
    cini(n);
    cout<<pl(dp[n][0], dp[n][1])<<endl;
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
