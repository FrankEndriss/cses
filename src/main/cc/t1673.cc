/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Bellman-Ford, find a negative cycle in oriented graph.
 * see https://cp-algorithms.com/graph/finding-negative-cycle-in-graph.html
 */
struct BFEdge {
    int a, b, cost;
    BFEdge(int aa, int bb, int cc):a(aa),b(bb),cost(cc) {
    };
};

/* return a vector with vertex of a negative cycle,
 * or empty vector for 'no negative cylcle exists'.
 * n is number of vertex
 */
vector<int> bellmanford(const int n, vector<BFEdge> &edges) {
    vector<int> d(n);
    vector<int> p(n, -1);
    int x;
    for (int i = 0; i < n; ++i) {
        x = -1;
        for (BFEdge e : edges) {
            if (d[e.a] + e.cost < d[e.b]) {
                d[e.b] = d[e.a] + e.cost;
                p[e.b] = e.a;
                x = e.b;
            }
        }
    }

    vector<int> ans;
    if (x == -1) {
        return ans;
    } else {
        for (int i = 0; i < n; ++i)
            x = p[x];

        for (int v = x;; v = p[v]) {
            ans.push_back(v);
            if (v == x && ans.size() > 1)
                break;
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
}

/*
 * First check if there is an infinite positive loop.
 * For that, utilize Bellman-Ford to find a negative cycle
 * on negate cost graph.
 * But, we may choose to use only that vertex that are reacheable
 * from end n-1, dfs them on reverse graph.
 *
 * Then consider cases that end is not reached at all, or
 * there exists such cycle,
 * or find best path with dijkstra.
 *
 *
 * Why WA?
 **/
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);

    vector<vector<pii>> adj(n);
    vvi adjR(n);

    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        cini(c);
        adj[a].emplace_back(b,c);
        adjR[b].push_back(a);
    }

    /* mark vertex that can reach end at n-1 */
    vb vis(n);
    function<void(int)> dfs=[&](int v) {
        vis[v]=true;
        for(auto chl : adjR[v])
            if(!vis[chl])
                dfs(chl);
    };
    dfs(n-1);
    assert(vis[0]);

    /* mark vertex that can be reached by 0 */
    vb vis2(n);
    function<void(int)> dfs2=[&](int v) {
        vis2[v]=true;
        for(auto chl : adj[v])
            if(!vis2[chl.first])
                dfs2(chl.first);
    };
    dfs2(0);
    assert(vis2[n-1]);

    vector<BFEdge> edg;
    for(int i=0; i<n; i++) {
        if(vis[i] && vis2[i])
            for(auto chl : adj[i])
                if(vis[chl.first] && vis2[chl.first])
                    edg.emplace_back(i, chl.first, -chl.second);
    }

    vi cyc=bellmanford(n, edg);
    if(cyc.size()>0)
        cout<<-1<<endl;
    else {
        queue<pii> q;  /* cost,vertex */
        q.emplace(0,0);
        vi dp(n,-INF);
        dp[0]=0;

        while(q.size()) {
            auto [d,v]=q.front();
            q.pop();

            if(dp[v]!=d)
                continue;

            for(auto chl : adj[v]) {
                if(vis[chl.first] && dp[chl.first]<dp[v]+chl.second) {
                    dp[chl.first]=dp[v]+chl.second;
                    q.emplace(dp[chl.first], chl.first);
                }
            }
        }
        cout<<dp[n-1]<<endl;
    }

}

signed main() {
    solve();
}
