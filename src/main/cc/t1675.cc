/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

inline void fs(int &number) {
#define gc() getchar_unlocked()

    int c = gc();
    while (c == ' ' || c == 13 || c == 10)
        c = gc();

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }
}

/**
 */
void solve() {
    int n,m;
    fs(n);
    fs(m);

    vector<tuple<int,int,int>> q;
    for(int i=0; i<m; i++) {
        int a,b,c;
        fs(a);
        fs(b);
        fs(c);

        q.emplace_back(c,a-1,b-1);
    }
    sort(all(q));

    dsu d(n);
    int cnt=0;
    ll ans=0;
    for(int i=0; cnt<n-1 && i<m; i++) {
        auto [c,a,b]=q[i];
        if(!d.same(a,b)) {
            d.merge(a,b);
            cnt++;
            ans+=c;
        }
    }
    if(cnt<n-1)
        cout<<"IMPOSSIBLE"<<endl;
    else
        cout<<ans<<endl;
}

signed main() {
        solve();
}
