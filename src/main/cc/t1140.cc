
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using t3=tuple<int,int,int>;
void solve() {
    cini(n);

    vector<t3> a;
    map<int,int> m;
    for(int i=0; i<n; i++) {
        cini(ai);
        cini(bi);
        cini(pi);
        a.emplace_back(bi, ai, pi);
        m[ai]=0;
        m[bi]=0;
    }

    int idx=1;
    for(auto it=m.begin(); it!=m.end(); it++)
        it->second=idx++;

    for(int i=0; i<n; i++) {
        a[i]={ m[get<0>(a[i])],
             m[get<1>(a[i])],
             get<2>(a[i]) };
    }

    sort(all(a));
    vi dp(idx);
    int ii=0;
    for(int i=1; i<idx; i++) {
        dp[i]=dp[i-1];
        while(ii<n && get<0>(a[ii])<=i) {
            dp[i]=max(dp[i], dp[get<1>(a[ii])-1]+get<2>(a[ii]));
            ii++;
        }
    }
    cout<<dp.back()<<endl;

    
}

signed main() {
    solve();
}
