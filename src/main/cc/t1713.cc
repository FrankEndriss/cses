/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>ll;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define endl "\n"

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    fori(t) {
        cini(x);
        if(x==1) {
            cout<<1<<endl;
            continue;
        }
        int ans=2;  // 1 and x

        for(int i=2; i*i<=x; i++) {
            if(x%i==0) {
                if(i*i==x)
                    ans++;
                else
                    ans+=2;
            }
        }
        cout<<ans<<endl;
    }

}

