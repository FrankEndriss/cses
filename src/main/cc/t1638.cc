/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const int N=1001;
int dp[N][N];
string g[N];

int main() {
    int n;
    cin>>n;
    for(int i=0; i<n; i++)
        cin>>g[i];

    if(g[0][0]!='*')
        dp[0][0]=1;

    for(int i=1; i<n; i++) {
        if(g[0][i]=='*')
            dp[0][i]=0;
        else
            dp[0][i]=dp[0][i-1];

        if(g[i][0]=='*')
            dp[i][0]=0;
        else
            dp[i][0]=dp[i-1][0];
    }

    const int MOD=1e9+7;
    for(int r=1; r<n; r++) {
        for(int i=1; i<n; i++) {
            if(g[r][i]=='*')
                dp[r][i]=0;
            else
                dp[r][i]=(dp[r-1][i]+dp[r][i-1])%MOD;
        }
    }

    cout<<dp[n-1][n-1]<<endl;
}

