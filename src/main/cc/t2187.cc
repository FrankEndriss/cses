
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* First if the given CBS is invalid ans==0.
 * Else the given CBS has a number of notclosed bracked, let that be nc.
 * Let sz be the number of additional open brackets we have to set.
 *
 * So we create (like stars'n'bars) CBS in between those notclosed brackets,
 * each of size 0..sz, sum of the sizes is sz.
 *
 * How to calculate that?
 * The number of different distributions is calculated simply by SnB formular,
 * but we need to multiplicate this formular by the numbers of the used
 * stars sizes. 
 * let nn(i) be the number of CBS of len 2*i.
 * ie if nc==2, and sz==2, we have
 * 2|0|0    ==3 * nn(2) ==6
 * 1|1|0    ==3 * nn(1) ==3 -> 6+3==9
 */
void solve() {
}

signed main() {
    solve();
}
