/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

const long MOD=1e9+7;
int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;

    int x=(n*(n+1))/2;
    if(x%2==1) {
        cout<<0<<endl;
        return 0;
    }
    x/=2;

    vector<ll> dp(x+1);
    dp[0]=1;
    for(int i=1; i<=n; i++)
        for(int j=x; j>=i; j--)
            dp[j]=(dp[j]+dp[j-i])%(2*MOD);

#ifdef DEBUG
for(int i=0; i<x+1; i++) 
cout<<dp[i]<<" ";
cout<<endl;
#endif

    cout<<(dp[x]/2)%MOD<<endl;
}

