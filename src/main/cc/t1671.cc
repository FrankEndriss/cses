/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<ll, int> plli;
typedef pair<int, int> pii;
typedef vector<pii> vpii;
typedef vector<vpii> vvpii;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vvpii tree(n);

    fori(m) {
        int a, b, c;
        cin>>a>>b>>c;
        a--; b--;
        tree[a].push_back({b, c});
    }

    const ll INF=1e18;
    vector<ll> cost(n, INF);
    priority_queue<plli> q;
    q.push({0, 0});
    cost[0]=0;
    vector<bool> vis(n);
    while(q.size()>0) {
        int node=q.top().second;
        q.pop();
        if(vis[node])
            continue;
        vis[node]=true;
        for(auto c : tree[node]) {
            ll lcost=cost[node]+c.second;
            if(lcost<cost[c.first]) {
                cost[c.first]=lcost;
                q.push({ -lcost, c.first});
            }
        }
    }

    for(auto c : cost) 
        cout<<c<<" ";
    cout<<endl;
}

