
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;



const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int;
using F=int;

S st_op(S a, S b) {
    return max(a,b);
}

/* This is the neutral element */
const S INF=1e9;
S st_e() {
    return -INF;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return max(f,x);
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return max(f,g);
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return -INF;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * Process queries from right to left a.
 * Foreach, update a lazy segtree to current value in range up to next bigger value.
 * Query sum and output diff to orig prefix-sum.
 *
 * How to handle the "increasing" part?
 * We could simply subtract 1,2,3,... from each position, then having not 
 * increaseing, but non-decreasing. So the updates are much more simple.
 *
 * other sol see:
 *  https://usaco.guide/problems/cses-2416-increasing-array-queries/solution
 */
void solve() {
}

signed main() {
    solve();
}
