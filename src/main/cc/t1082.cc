
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int MOD=1e9+7;
using mint=modint1000000007;

mint gauss(int i) {
    mint ans=mint(i)*mint(i+1)/2;
    //cerr<<"gauss("<<i<<")="<<ans<<endl;
    return ans;
}

/* Of the number from 1..n, each one contributes
 * (n/i)*i, since there are n/i multiples of it.
 * (n/i)*i == n-(n%i)
 *
 * Consider the numbers j=1..sqrt(n)
 * Contribution==(n-n%i)*i
 * Also we got cnt=n/i - n/(i+1) numbers where contribution is
 * gauss(n/i)-gauss(n/(i+1))
 * n%(n/i), n%(n/i)+i, n%(n/i)+2*i,...
 * = gauss(cnt-1)*i + (n%(n/i))*cnt
 */
void solve() {
    cini(n);

    mint ans=0;
    for(int i=1; i*i<=n; i++) {
        const int a1=(n/i)*i;
        ans+=a1;

        if(i<n/i) {
            const mint a2=gauss(n/i) - gauss(n/(i+1));
            //cerr<<"i="<<i<<" a1="<<a1<<" a2="<<a2<<endl;
            ans+=(a2*i);
        } else  {
            //cerr<<"i="<<i<<" a1="<<a1<<endl;
        }
    }
    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
