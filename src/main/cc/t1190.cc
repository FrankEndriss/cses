/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;



template<typename E>
struct SegmentTreeLazy {
    vector<E> data;

    E neutral;
    int n;

    function<E(E,E)> plus;

    SegmentTreeLazy(int _n, E _neutral) {
        n=1;
        while(n<_n)
            n*=2;

        data.resize(n*2, _neutral);
        neutral=_neutral;
    }

    /* bulk update in O(n) */
    template<typename Iterator>
    void init(Iterator beg, Iterator end) {
        for(int idx=n; beg!=end; beg++, idx++)
            data[idx]=*beg;

        for(int idx=n-1; idx>=1; idx--)
            data[idx]=plus(data[idx*2], data[idx*2+1]);
    }

    /* @return accumulative (l,r), both inclusive, top down */
    E query(int node, int sL, int sR, int l, int r) {
        if (r < sL || l > sR)
            return neutral;
        //cerr<<"query, node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" data[node]="<<data[node]<<endl;

        if (l<=sL && r>=sR)
            return data[node];

        int mid = (sL+sR)/2;
        return plus(query(node*2, sL, mid, l, r), query(node*2+1, mid+1, sR, l, r));
    }
    E query(int l, int r) {
        return query(1, 0, n-1, l, r);
    }

    /* set val at position */
    void set(int idx, E val) {
        idx+=n;
        data[idx]=val;
        while(idx>1) {
            idx/=2;
            data[idx]=plus(data[idx*2], data[idx*2+1]);
        }
    }
};


void solve() {
    struct node {
        int pref;   // maximum prefix sum
        int post;   // maximum postfix sum
        int msum;   // maximum sumbarray sum
        int val;    // sum of all values
    };

    cini(n);
    cini(m);

    vector<node> data(n);
    for(int i=0; i<n; i++) {
        cin>>data[i].val;
        data[i].pref=max(0LL, data[i].val);
        data[i].post=max(0LL, data[i].val);
        data[i].msum=max(0LL, data[i].val);
    }

    node neutral;   // all zero
    neutral.pref=neutral.post=neutral.msum=neutral.val=0;

    SegmentTreeLazy<node> seg(n, neutral);
    
    seg.plus=[](node lef, node rig) {
            node ans;
            ans.val=lef.val+rig.val;
            ans.pref=max(lef.pref, lef.val+rig.pref);
            ans.post=max(rig.post, lef.post+rig.val);
            ans.msum=max(lef.msum, rig.msum);
            ans.msum=max(ans.msum, lef.post+rig.pref);
            return ans;
    };

    seg.init(all(data));

    for(int i=0; i<m; i++) {
        cini(k);
        cini(x);
        node nnode;
        nnode.val=x;
        nnode.pref=max(0LL, nnode.val);
        nnode.post=max(0LL, nnode.val);
        nnode.msum=max(0LL, nnode.val);
        seg.set(k-1, nnode);

        cout<<seg.query(0,n-1).msum<<endl;
    }


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
