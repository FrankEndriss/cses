/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


/* see https://cp-algorithms.com/data_structures/sqrt_decomposition.html#toc-tgt-4
 * Queries are inclusive l and inclusive r.
 **/
//const int BLOCK_SIZE=(int)sqrt(2e5+5)+1;

const int BLOCK_SIZE=512;

bool cmp(pair<int, int> p, pair<int, int> q) {
    if (p.first / BLOCK_SIZE != q.first / BLOCK_SIZE)
        return p < q;
    return (p.first / BLOCK_SIZE & 1) ? (p.second < q.second) : (p.second > q.second);
}

struct Query {
 /* inclusive l and inclusive r */
    int l, r, idx;

    bool operator<(Query other) const {
        return cmp(make_pair(l, r), make_pair(other.l, other.r));
    }
};

/* implement updates and query */
void moremove(int);
void moadd(int);
int moget_answer();

vector<int> mo(vector<Query>& queries) {
    vector<int> answers(queries.size());
    sort(queries.begin(), queries.end());
    int cur_l = 0;
    int cur_r = -1;

    /* invariant: data structure will always reflect the range [cur_l, cur_r] */
    for (Query q : queries) {
        while (cur_l > q.l) {
            cur_l--;
            moadd(cur_l);
        }
        while (cur_r < q.r) {
            cur_r++;
            moadd(cur_r);
        }
        while (cur_l < q.l) {
            moremove(cur_l);
            cur_l++;
        }
        while (cur_r > q.r) {
            moremove(cur_r);
            cur_r--;
        }
        answers[q.idx] = moget_answer();
    }
    return answers;
}

const int N=2e5+3;
vi aa;
vi freq(N);
int cnt;

void moremove(int idx) {
    if(freq[aa[idx]]==1)
        cnt--;
    freq[aa[idx]]--;
}

void moadd(int idx) {
    if(freq[aa[idx]]==0)
        cnt++;
    freq[aa[idx]]++;
}

int moget_answer() {
    return cnt;
}

/* 
 * init O(logn):
 * We find for each r[i] the next same elemet right of it.
 * let r[i]== the min(j) of the elements a[j]==a[i] and j>i
 *
 * query(l,r]:
 * let d=number of r[k] in range (l,r] with r[k]<r
 * then ans=r-l-d
 *
 * Find d in O(log(n)^2) with merge sort ranges.
 */
void solve() {
    cini(n);
    cini(q);

    aa.resize(n);
    map<int,int> m;
    for(int i=0; i<n; i++) {
        cini(a);
        auto it=m.find(a);
        if(it!=m.end())
            aa[i]=it->second;
        else {
            int num=m.size();
            m[a]=num;
            aa[i]=num;
        }
    }

    vector<Query> quer(q);

    for(int i=0; i<q; i++) {
        cin>>quer[i].l>>quer[i].r;
        quer[i].l--;
        quer[i].r--;
        quer[i].idx=i;
    }
    vi ans=mo(quer);

    for(int i=0; i<q; i++) 
        cout<<ans[i]<<endl;
}

signed main() {
    solve();
}

