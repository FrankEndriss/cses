
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;


const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

inline void fs(int &number) {
#define gc() getchar_unlocked()

    int c = gc();
    while (c == ' ' || c == '\t' || c == 13 || c == 10)
        c = gc();

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }
}

/* see http://www.zverovich.net/2013/09/07/integer-to-string-conversion-in-cplusplus.html
 * We could make it even faster using 3 (or 4?) digits per division. */
const char digits[] =
    "00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899";

#define pc(x) putchar_unlocked(x);
inline void writeInt(ll n) {
    char buf[32];
    int idx = 0;

    if (n == 0) {
        pc('0');
        return;
    }

    while (n >= 100) {
        ll val = n % 100;
        buf[idx++] = digits[val * 2 + 1];
        buf[idx++] = digits[val * 2];
        n /= 100;
    }

    while (n) {
        buf[idx++] = n % 10 + '0';
        n /= 10;
    }

    while (idx--) {
        pc(buf[idx]);
    }
}

using S=pii; /* price,position */

S st_opL(S a, S b) {
    int ya=a.first+a.second;
    int yb=b.first+b.second;
    if(ya<yb)
        return a;
    else
        return b;
}

S st_opR(S a, S b) {
    int ya=a.first-a.second;
    int yb=b.first-b.second;
    if(ya<yb)
        return a;
    else
        return b;
}

const int INF=2e9;
S st_e() {
    return { INF, 0 };
}

/* The price k[i] of a pizza in house i is min of
 * k[i-1]+1
 * k[i+1]+1
 * p[i]
 *
 * Note that the set of houses where a pizzeria is
 * the cheapest pizzeria is a contigous subarray.
 * So if the price changes, that subarray get smaller or bigger
 * on each end.
 * But, we cannot maintain something per position.
 * ...
 * Of all pizzerias left of a house,
 * the ones price function which intersects the vert-axis at the lowest
 * point is the one giving the lowest price.
 * Same true for right of a house pizerias,
 * the ones price function which intersects the vert-axis at the lowest
 * point is the one giving the lowest price.
 * So we maintain two min-segment trees, one for left, one for right.
 * In the tree we store position and prize of the pizzeria.
 */
void solve() {
    signed n,q;
    fs(n);
    fs(q);

    vector<pii> a(n);
    for(int i=0; i<n; i++) {
        fs(a[i].first);
        a[i].second=i;
    }

    segtree<pii, st_opL, st_e> segL(a);
    segtree<pii, st_opR, st_e> segR(a);

    for(int i=0; i<q; i++) {
        signed t;
        fs(t);
        if(t==1) {
            signed k,x;
            fs(k);
            k--;
            fs(x);
            segL.set(k,{x,k});
            segR.set(k,{x,k});
        } else if(t==2) {
            signed k;
            fs(k);
            k--;
            pii m1=segL.prod(k,(signed)n);
            pii m2=segR.prod(0,k+1);
            int ans=min(m1.first-k+m1.second, m2.first+k-m2.second);
            writeInt(ans);
            pc('\n');
        } else
            assert(false);
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
