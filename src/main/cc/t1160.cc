/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * All paths end in a loop.
 * But not all vertices are part of a loop.
 *
 * Let c be the first vertex in (a,b) which is
 * element of a loop, then the length of the path (a,b) is 
 * length of (a,c)+(c,b)
 *
 * So we want to group the vertices by "path into loop"- and
 * "loop"- components.
 *
 * The "path into loop" all start at a vertex with no
 * incoming edge.
 * Note that there can be "loop" without any path leading
 * into that loop.
 */
void solve() {
    cini(n);
    cini(q);

    cinai(t,n);
    vi icnt(n); /* incoming count */
    for(int i=0; i<n; i++) {
        t[i]--;
        icnt[t[i]]++;
    }

    vi pintoidx(n, -1); /* pintoidx[i]= index of path in pinto vertex i is part of */
    vector<map<int,int>> pinto;      /* pinto[i][j]= position of vertex j in ith "path into loop" */
    vector<map<int,int>> ploop;      /* ploop[i][j]= position of vertex j in ith "loop" */

    vb vis(n);
    for(int i=0; i<n; i++) {
        if(icnt[i]>0)
            continue;

        int v=i;
        int pos=0;
        pinto.resize(pinto.size()+1);
        do {
            pinto.back()[v]=pos;
            pos++;
            vis[v]=true;
            v=t[v];
        }while(!vis[v]);

        // ...TODO

    }

    vb vis(n);
    vector<map<int,int>> cpos; /* cpos[i][j]=position of j in ith component */
    vi comp(n);

    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;

        cpos.resize(cpos.size()+1);

        int v=i;
        int pos=0;
        do {
            vis[v]=true;
            comp[v]=cpos.size()-1;
            cpos.back()[v]=pos;
            pos++;
            v=t[v];
        }while(v!=i);
    }

    for(int i=0; i<q; i++) {
        cini(a); a--;
        cini(b); b--;

        int c=comp[a];
        if(c!=comp[b]) {
            cout<<-1<<endl;
            continue;
        }

        int ans=abs(cpos[c][a]-cpos[c][b]);
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
