
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * At each position, we got foreach possible with a max hight.
 * So we would need to extends the list of the above cell
 * by the current row,
 * and the list of the left cell by the current col.
 *
 * Use min left/right, created by stack technique.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cinas(s,n);

    /** to[i][j]= number of free cells at position i,j in direction North */
    vvi to(n, vi(m));

    int ans=0;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(s[i][j]=='.') {
                to[i][j]=1;
                if(i>0)
                    to[i][j]+=to[i-1][j];
            }
        }

        /** now check current row, foreach position find
         * the position to left (and right) where the value is smaller
         * than current position.*/

        stack<pii> st;
        st.emplace(-1, -1);
        vi le(m);
        for(int j=0; j<m; j++) {
            while(st.top().first>=to[i][j])
                st.pop();
            le[j]=st.top().second;
            st.emplace(to[i][j], j);
        }

        while(st.size())
            st.pop();
        st.emplace(-1, m);
        vi re(m);
        for(int j=m-1; j>=0; j--) {
            while(st.top().first>=to[i][j])
                st.pop();
            re[j]=st.top().second;
            st.emplace(to[i][j], j);
        }

        for(int j=0; j<m; j++) 
            ans=max(ans, to[i][j]*(j-le[j]+re[j]-j-1));

    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
