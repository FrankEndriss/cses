/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

int main() {
    string s;
    cin>>s;
    vector<int> f(256);
    for(char c : s)
        f[c]++;

    string ans;
    int cnt=0;
    char fc;
    for(char i='A'; i<='Z'; i++) {
        while(f[i]>1) {
            ans+=i;
            f[i]-=2;
        }
        if(f[i]) {
            cnt++;
            fc=i;
        }
    }

    if(cnt>1) 
        ans="NO SOLUTION";
    else {
        if(cnt==1)
            ans+=fc;
        for(int i=ans.size()-cnt-1; i>=0; i--)
            ans+=ans[i];
    }
    cout<<ans<<endl;
}

