#include <bits/stdc++.h>
const int MOD=1000000007;
int main() {
    long long w[1000001]={0,1,2,4,8,16,32};
    int n;
    std::cin>>n;
    for(int i=7; i<=n; i++) {
        for(int j=1; j<=6; j++)
            w[i]+=w[i-j];
        w[i]%=MOD;
    }
    std::cout<<w[n]<<"\n";
}

