
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(6);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Since n and r are small some brute force should work.
 * The expected number of inversions of two positions
 * is:
 * Number of all pairs where left is bigger div number of 
 * all pairs.
 * ans is the sum of that number for all pairs of positions.
 */
void solve() {
    cini(n);
    cinai(r,n);

    ld ans=0;
    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            int cnt=0;
            for(int k=2; k<=r[i]; k++)
                cnt+=min(k-1, r[j]);
            ld cntAll=r[i]*r[j];
            ans+=cnt/cntAll;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
