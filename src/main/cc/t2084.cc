

/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return min(a,b);
}

const S INF=1e18;
S st_e() {
    return INF;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Note that we 'jump' to skill factor f[i] by 
 * killing the s[i] monster.
 *
 * We arrive at the last monster at time t0 while having skill factor f[j].
 * We got this skill factor by arriving at s[j] at some t1.
 *
 * Consider arriving at s[j] at time t1 with skill factor f[k]. Then
 * ans=t1+f[k]*s[j]+f[j]*s[n-1]
 * So, this means overall
 * dp[i]=min time to gain skill level i, then
 * ans=min(dp[i]+f[i]*s[n-1]), or simply
 * ans=dp[n-1], since killing last monster means gaining skill level n-1
 *
 * dp[i]=min(dp[0..i-1]+s[i]*f[0..i-1]])
 * How to get that in less than O(N^2)?
 */
void solve() {
    cini(n);
    cini(x);

    cinai(s,n);
    cinai(f,n);

    //stree seg(n);
    //seg.set(0, s[0]*x);
    vi dp(n,INF);

    for(int i=0; i<n; i++) {
        dp[i]=x*s[i];
        for(int j=0; j<i; j++)
            dp[i]=min(dp[i], dp[j]+s[i]*f[j]);
    }

    cout<<dp[n-1]<<endl;
}

signed main() {
        solve();
}
