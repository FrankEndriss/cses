
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Max-Flow with Edmonds-Karp
 * see https://cp-algorithms.com/graph/edmonds_karp.html */
const int INF = 1e18;

int bfs(int s, int t, vi &parent, vvi &adj, vvi &capacity) {
	fill(parent.begin(), parent.end(), -1);
	parent[s] = -2;
	queue<pii> q;
	q.push( { s, INF });

	while (!q.empty()) {
		int cur = q.front().first;
		int flow = q.front().second;
		q.pop();

		for (int next : adj[cur]) {
			if (parent[next] == -1 && capacity[cur][next]) {
				parent[next] = cur;
				int new_flow = min(flow, capacity[cur][next]);
				if (next == t)
					return new_flow;
				q.push( { next, new_flow });
			}
		}
	}

	return 0;
}

/* s==source
 * t==sink
 * n==adj.size()
 * adj is standart adjency matrix
 * NOTE: capacity is n*n matrix, only positive capacity is inserted
 * NOTE: adj is unidirected, ie both directions must be inserted!
 */
int maxflow(int s, int t, vvi &adj, vvi &capacity, int n) {
	int flow = 0;
	vi parent(n);
	int new_flow;

	while ((new_flow = bfs(s, t, parent, adj, capacity))) {
		flow += new_flow;
		int cur = t;
		while (cur != s) {
			int prev = parent[cur];
			capacity[prev][cur] -= new_flow;
			capacity[cur][prev] += new_flow;
			cur = prev;
		}
	}

	return flow;
}

void solve() {
    cini(n);
    cini(m);
    vvi adj(n);
    vvi cap(n, vi(n));

    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        cini(c);
        adj[u].push_back(v);
        adj[v].push_back(u);
        cap[u][v]+=c;
    }

    int ans=maxflow(0,n-1,adj,cap, n);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
