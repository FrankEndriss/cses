
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

#pragma GCC target("avx2")

/* Foreach cell=='1' we need to to check if it is the
 * say NW corner of a rect with 4 black corners.
 * This is, the number of black cells right of current
 * cell 
 *
 * It seems an implementation based on int and
 * __buildin_popcount() runs faster.
 * see https://cses.fi/book/book.pdf#page=111
 * But for some reasong that seems to notwork.
 * Statistics imply that it should run in 25ms, which is
 * aprox O(n^2)
 */
const int N=3000;
const int B=60;
ll rows[(N+B-1)/B][N];

/* for some reason that does not work with B==60, but
 * it works for B==30
 */
void solve() {
    cini(n);
    cinas(s,n);

    const int bckt=(n+B-1)/B;
    //vvi rows(n, vi(bckt));
    for(int i=0; i<n; i++)  {
        for(int j=0; j<bckt; j++) {
            for(int k=0; k<B && j*B+k<n; k++) {
                if(s[i][j*B+k]=='1')
                    rows[j][i]|=(1LL<<k);
            }
        }
    }

    ll ans=0;
    for(int i=0; i<n; i++) 
        for(int j=i+1; j<n; j++)  {
            ll cnt=0;
            for(int k=0; k<bckt; k++)
                cnt+=__builtin_popcount(rows[k][i]&rows[k][j]);

            ans+=cnt*(cnt-1)/2;
        }


    cout<<ans<<endl;
}

signed main() {
    solve();
}
