
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider row by row.
 * We allways put tiles so that all cells of a row are covered,
 * and some cells off the next row. There are 2^10-1 possible different
 * coverings of that next row.
 * Foreach of them there are a number of possible coverings for the next
 * row, and coverings for the row after the next row.
 *
 * dp[i]=possibilities of covering the previous row completly and 
 *   current row with mask i.
 * init
 * dp[0]=1;
 */
const int MOD=1e9+7;
void solve() {
    cini(n);
    cini(m);

    /* create a table of following covering states for a given covering state. 
     * dp1[i]=list of states possibly following state i
     */
    vvi dp1(1<<n);
    for(int i=0; i<(1<<n); i++) {

        function<void(int,int)> go=[&](int fstate, int bit) {
            if(bit==n) {
                dp1[i].push_back(fstate);
                return;
            }
            const int b=1<<bit;
            if(i&b) { /* if bit is set we cannot set in following state */
                go(fstate, bit+1);
            } else { /* bit is not set in i*/
                go(fstate|b, bit+1); /* vertical tile */
                if(bit+1<n && (i&(b<<1))==0)  { /* next bit also not set */
                    go(fstate,bit+2);  /* horizontal tile */
                }
            }
        };

        go(0, 0);
    }

    /* dp[i]=number of possibilities of covering i of current row after prev row 
     * is covered completly. */
    vi dp(1<<n);
    dp[0]=1;
    for(int i=0; i<m; i++) {
        vi dp0(1<<n);
        for(int j=0; j<(1<<n); j++) {
            for(int k : dp1[j]) {
                dp0[k]+=dp[j];
                if(dp0[k]>=MOD)
                    dp0[k]-=MOD;
            }
        }
        dp.swap(dp0);
    }
    cout<<dp[0]<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
