#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

const int N=1000001;
const int INF=1000000007;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, x;
    cin>>n>>x;
    vector<int> c(n);
    vector<int> ans(N);
    fori(n) {
        cin>>c[i];
        ans[c[i]]=1;
    }

    function<int(int)> ok=[&](int v) {
        if(v<0)
            return -1;
        if(v==0)
            return 0;
        if(ans[v]!=0)
            return ans[v];

        int mi=INF;
        for(int i=0; i<n; i++) {
            int o=ok(v-c[i]);
            if(o>=0)
                mi=min(mi, o);
        }
        if(mi!=INF)
            ans[v]=mi+1;
        else
            ans[v]=-1;
        return ans[v];
    };

    cout<<ok(x)<<endl;
}

