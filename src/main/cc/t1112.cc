/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

constexpr int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

/* Known as the prefix function. See
 * https://cp-algorithms.com/string/prefix-function.html
 * http://algorithmsforcontests.blogspot.com/2012/08/borders-of-string.html
 * @return len of border pre/postfix in O(n)
 **/
int calculate_border(string& s) {
    int i = 1;
    int j = -1;
    int n = (int)s.size();

    vi border(s.size());
    border[0] = -1;
    while(i < n) {
        while(j >= 0 && s[i] != s[j+1])
            j = border[j];
        if (s[i] == s[j+1])
            j++;
        border[i++] = j;
    }
    return border.back()+1;
}

/* calculate all borders of s */
vi all_border(string s) {

    vi ans;
    while(true) {
        int len=calculate_border(s);
        if(len<=0)
            break;
        ans.push_back(len);
        s=s.substr(0,len);
    }
    return ans;
}

/* Inclusion/exclusion principle and dp.
 *
 * Number of string with pattern m starting at position i:
 * p26[i]*p26[n-m-i]
 *
 * So let's go left to right and sum them up.
 * To anticipate an overcount we need to remove the number
 * of strings with the pattern left of the current position
 * and at the current position as we counted them before.
 *
 * Subtraction is the sum of all positions where the pattern can begin,
 * devided by the "unused" positions right of the begin position.
 * "Unused" positions are m-overlap
 * overlap is the border size.
 */
void solve() {
    cini(n);
    cins(s);
    const int m=s.size();

    vi p26(n+1);
    vi ip26(n+1);
    p26[0]=1;
    ip26[0]=inv(p26[0]);
    for(int i=1; i<=n; i++) {
        p26[i]=mul(p26[i-1],26);
        ip26[i]=inv(p26[i]);
    }

    vi vborders=all_border(s);
    vb borders(n);
    for(int v : vborders)
        borders[v]=true;

    /* dp[i]=number of strings having its first occ of pattern
     * starting at position i.
     */
    vi dp(n);
    int ans=0;

    for(int i=0; i+m<=n; i++) {
        dp[i]=mul(p26[i], p26[n-i-m]);

        for(int j=0; j+m<=i; j++)
            dp[i]=pl(dp[i], -mul(dp[j], ip26[m]));

        for(int b=1; b<m; b++) {    /* possible border sizes */
            if(i-m+b<0)   /* starting position left of 0 */
                continue;

            if(borders[b])
                dp[i]=pl(dp[i], -mul(dp[i-m+b], ip26[m-b]));
        }

        ans=pl(ans, dp[i]);
    }

    cout<<ans<<endl;

    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
