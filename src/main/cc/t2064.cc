
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


constexpr int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

const int N=2e6+3;
vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}
(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

/*
 * ***
 * If we put another 2 brackets into an existing seq, we have
 * several choices to do so.
 * It depends on how much independent seqs the previous one
 * exists.
 * We generaly can put a "()" at every position, but we have
 * to admit that that creates doubles :/
 * ***
 * Lets find another model of a bs
 * remove each ")" and subst the "(" by the number of positions
 * before the ")" is there. Examples:
 * () -> becomes 1
 * ()() -> 11
 * (()) -> 21
 * ()(()) -> 121
 * Generaly, a seq looks like this: i,i-1,...,1,j,j-1,...,1
 * So, we can insert a 1 at each segment of 1s, or
 * at begin(), if the seq does not start with 1.
 * And we can insert a i+1 before each i.
 *
 * 1 -> 11, 21
 * 11 -> 111, 211, 121
 * 21 -> 121, 321, 211
 * ....
 * 111...1111  1
 * 111...1121  ...
 * 111...1211  n-1
 * ...
 * 111...1321   ...
 * 111...3211   n-2
 * Then lets further subtitute each 21 by 2 and 321 by 3 etc.
 *
 * Then we can basically subtitute each sequence by a 1 shorter sequence
 * by adding two adjacent numbers.
 * 11111 -> 1121 -> 221 (examples)
 *       -> 2111 -> 221
 *       -> 1112 -> 212
 * ***
 * See https://cp-algorithms.com/combinatorics/bracket_sequences.html
 */
void solve() {
    cini(n);

    if(n&1) {
        cout<<0<<endl;
    } else {
        const int i=inv(n/2+1);
        int ans=mul(i, nCr(n, n/2));
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}
