
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

inline void fs(int &number) {
#define gc() getchar_unlocked()

    int c = gc();
    while (c == ' ' || c == '\t' || c == 13 || c == 10)
        c = gc();

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }
}

/* it is somehow the number of inversions.
 * consider 1 2 3 4 5, which is one round. Then swap 2,4
 * 1 4 3 2 5
 * 1 2; 3; 4 5;
 *
 * With each swap 2 numbers are moved, we need to consider
 * if and how these two moves change the position of the numbers
 * in respect of the pre and post numbers.
 * Each change contributes 1 or -1.
 */
void solve() {
    int n;
    fs(n);

    vi a(n);
    vi pos(n);

    for(int i=0; i<n; i++) {
        fs(a[i]);
        a[i]--;
        pos[a[i]]=i;
    }

    int ans=1;
    for(int i=0; i+1<n; i++) 
        if(pos[i]>pos[i+1])
            ans++;

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
