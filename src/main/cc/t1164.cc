/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<pair<int, int>> evt;
    for(int i=1; i<=n; i++) {
        int a, b;
        cin>>a>>b;

        evt.push_back({ a, -i });
        evt.push_back({ b, i });
    }

    vector<int> cust(n+1);    // rooms of customers
    stack<int> freeRooms;
    for(int i=n; i>0; i--)
        freeRooms.push(i);

    sort(evt.begin(), evt.end());
    int maxRoom=0;
    for(auto e : evt) {
        if(e.second<0) {  // arrival
            int room=freeRooms.top();
            freeRooms.pop();
            cust[-e.second]=room;
            maxRoom=max(maxRoom, room);
        } else {
            int room=cust[e.second];
            freeRooms.push(room);
        }
    }

    cout<<maxRoom<<endl;
    for(int i=1; i<=n; i++)
        cout<<cust[i]<<" ";
    cout<<endl;
}

