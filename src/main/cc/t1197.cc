
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Bellman-Ford, find a negative cycle in oriented graph.
 * see https://cp-algorithms.com/graph/finding-negative-cycle-in-graph.html
 */
struct BFEdge {
    int a, b, cost;
};

/* return a vector with vertex of a negative cycle,
 * or empty vector for 'no negative cylcle exists'.
 * n is number of vertex
 */
vector<int> bellmanford(const int n, vector<BFEdge> &edges) {
    vector<int> d(n);
    vector<int> p(n, -1);
    int x;
    for (int i = 0; i < n; ++i) {
        x = -1;
        for (BFEdge e : edges) {
            if (d[e.a] + e.cost < d[e.b]) {
                d[e.b] = d[e.a] + e.cost;
                p[e.b] = e.a;
                x = e.b;
            }
        }
    }

    vector<int> ans;
    if (x == -1) {
        return ans;
    } else {
        for (int i = 0; i < n; ++i)
            x = p[x];

        for (int v = x;; v = p[v]) {
            ans.push_back(v);
            if (v == x && ans.size() > 1)
                break;
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
}
/**
 * Bellman-Ford
 */
void solve() {
    cini(n);
    cini(m);

    vector<BFEdge> edg(m);
    for(int i=0; i<m; i++) {
        cin>>edg[i].a>>edg[i].b>>edg[i].cost;
        edg[i].a--;
        edg[i].b--;
    }

    vi cyc=bellmanford(n, edg);
    if(cyc.size()==0) {
        cout<<"NO"<<endl;
    } else {
        cout<<"YES"<<endl;
        for(int i : cyc)
            cout<<i+1<<" ";
        cout<<endl;
    }

}

signed main() {
    solve();
}
