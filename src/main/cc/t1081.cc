/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>ll;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define endl "\n"

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    const int N=1000001;

    vi dp(N);  // dp[i]==times i was found as divisor of some a[k]

    cini(n);
    for(int k=0; k<n; k++) {
        int a;
        cin>>a;

        for(int i=1; i*i<=a; i++) {
            if(a%i==0) {
                dp[i]++;
                int j=a/i;
                if(j!=i)
                    dp[j]++;
            }
        }
    }

    for(int i=N-1; i>=1; i--) {
        if(dp[i]>1) {
            cout<<i<<endl;      
            return 0;
        }
    }
    assert(false);
}

