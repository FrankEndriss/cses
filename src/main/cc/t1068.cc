
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>

int main() {
    long long n;
    std::cin>>n;
    
    while(n!=1) {
        std::cout<<n<<" ";
        n=n&1 ?  n*3+1 : n/2;
    }
    std::cout<<"1\n";

}

