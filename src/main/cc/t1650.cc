/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, q, a, b;
    cin>>n>>q;
    vi x(n);
    int last=0;
    fori(n) {
        int t;
        cin>>t;
        x[i]=last^=t;
    }

    fori(q) {
        cin>>a>>b;
        int left=a==1?0:x[a-2];
        cout<<(left^(x[b-1]))<<endl;
    }
}

