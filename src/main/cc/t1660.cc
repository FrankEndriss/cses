/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

typedef vector<int> vi;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, x;
    cin>>n>>x;
    vi a(n);
    fori(n)
    cin>>a[i];

    int l=0;
    int r=0;
    ll sum=a[0];
    int ans=0;
    while(r<n) {
        if(sum==x)
            ans++;

        if(sum>x && l<r) {
            sum-=a[l];
            l++;
        } else {
            r++;
            if(r<n)
                sum+=a[r];
        }
    }
    cout<<ans<<endl;

}

