
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/maxflow>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* use atcoders mf_graph.min_cut()
 */
const int INF=1e6;
void solve() {
    cini(n);
    cini(m);

    mf_graph<int> g(n+2);
    const int S=n;
    const int T=S+1;

    vector<pii> ab(m);
    for(int i=0; i<m; i++) {
        cini(a);
        cini(b);
        g.add_edge(a-1, b-1, 1);
        g.add_edge(b-1, a-1, 1);
        ab[i]={ a, b };
    }
    g.add_edge(S,0,INF);
    g.add_edge(n-1,T,INF);

    int ans=g.flow(S,T);
    auto b=g.min_cut(S);

    /* b is of size n 
     *
     * It seems it is a list of all vertex belonging
     * to the source component after cut.
     *
     * So we need to output all edges from vertex
     * v where b[v]==true to all vertex u where b[u]=false.
     * */
    cout<<ans<<endl;
    for(int i=0; i<m; i++) {
        if(b[ab[i].first-1] && !b[ab[i].second-1])
            cout<<ab[i].first<<" "<<ab[i].second<<endl;
    }
}

signed main() {
    solve();
}
