/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(6);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);
    cini(a);
    cini(b);

    /* dp[i]=number of ways to reach sum i */
    vd dp(601,-1);
    dp[0]=1;
    for(int i=0; i<n; i++) {
        vd dp2(601);
        for(int j=0; j+6<=600; j++) {
            if(dp[j]>=0) {
                for(int k=1; k<=6; k++)
                    dp2[j+k]+=dp[j];
            }
        }
        dp.swap(dp2);
    }

    double dsumall=accumulate(all(dp), 0.0);
    double ab=accumulate(dp.begin()+a, dp.begin()+b+1, 0.0);

    double ans=ab/dsumall;
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
