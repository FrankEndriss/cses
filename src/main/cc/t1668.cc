/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vvi tree(n);
    fori(m) {
        int a, b;
        cin>>a>>b;
        a--;
        b--;
        tree[a].push_back(b);
        tree[b].push_back(a);
    }
    
    vi colors(n);
    function<bool(int, int)> dfs=[&](int node, int col) {
        colors[node]=col;
        for(auto c : tree[node]) {
            if(colors[c]==col)
                return false;
            else if(colors[c]==0)
                if(!dfs(c, -col))
                    return false;
        }
        return true;
    };

    for(int i=0; i<n; i++) {
        if(colors[i]==0 && !dfs(i, 1))
            cout<<"IMPOSSIBLE"<<endl;
    }
    for(int i=0; i<n; i++)
        if(colors[i]==1)
            cout<<1<<" ";
        else
            cout<<2<<" ";
    cout<<endl;
}

