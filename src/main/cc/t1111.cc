
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* Utilize Manacher's algo.
 * see https://cp-algorithms.com/string/manacher.html
 * <pos,size>
 */
constexpr int N=1e6+7;
int d1[N];
pii manacher_d1(string &s) {
    const int n=s.size();
    pii ans={0,1};
    for (int i = 0, l = 0, r = -1; i < n; i++) {
        int k = (i > r) ? 1 : min(d1[l + r - i], r - i + 1);
        while (0 <= i - k && i + k < n && s[i - k] == s[i + k]) {
            k++;
        }
        d1[i] = k--;
        if(ans.second<d1[i]) {
            ans.first=i;
            ans.second=d1[i];
        }
        if (i + k > r) {
            l = i - k;
            r = i + k;
        }
    }
    return ans;
}

int d2[N];
pii manacher_d2(string &s) {
    const int n=s.size();
    pii ans={0,0};
    for (int i = 0, l = 0, r = -1; i < n; i++) {
        int k = (i > r) ? 0 : min(d2[l + r - i + 1], r - i + 1);
        while (0 <= i - k - 1 && i + k < n && s[i - k - 1] == s[i + k]) {
            k++;
        }
        d2[i] = k--;
        if(ans.second<d2[i]) {
            ans.first=i;
            ans.second=d2[i];
        }
        if (i + k > r) {
            l = i - k - 1;
            r = i + k ;
        }
    }
    return ans;
}

/* Utilize Manacher's algo.
 * see https://cp-algorithms.com/string/manacher.html
 */
void solve() {
    cins(s);
    pii d1=manacher_d1(s);
    pii d2=manacher_d2(s);

    if(d1.second*2-1>d2.second*2)
        cout<<s.substr(d1.first-d1.second+1, d1.second*2-1)<<endl;
    else
        cout<<s.substr(d2.first-d2.second, d2.second*2)<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
