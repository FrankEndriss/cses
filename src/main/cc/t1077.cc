/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

struct win {
    multiset<int> lo;    // smaller elements
    multiset<int> hi;    // bigger elements
    int sumlo=0;
    int sumhi=0;

    void norm() {
        while(lo.size()+1<hi.size()) {
            auto it=hi.begin();
            sumlo+=*it;
            sumhi-=*it;
            lo.insert(*it);
            hi.erase(it);
        }

        while(lo.size()>hi.size()) {
            auto it=lo.end();
            it--;
            sumlo-=*it;
            sumhi+=*it;
            hi.insert(*it);
            lo.erase(it);
        }

        while(lo.size()>0 && *lo.rbegin()>*hi.begin()) {
            auto itlo=lo.end();
            itlo--;
            hi.insert(*itlo);
            sumhi+=*itlo;
            sumlo-=*itlo;
            lo.erase(itlo);
            auto it=hi.begin();
            lo.insert(*it);
            sumlo+=*it;
            sumhi-=*it;
            hi.erase(it);
        }
    }

    void add(int a) {
        if(lo.size()<hi.size()) {
            lo.insert(a);
            sumlo+=a;
        } else {
            hi.insert(a);
            sumhi+=a;
        }
        norm();
    }

    void remove(int a) {
        if(a<*hi.begin()) {
            auto it=lo.find(a);
            assert(it!=lo.end());
            sumlo-=*it;
            lo.erase(it);
        } else {
            auto it=hi.find(a);
            assert(it!=hi.end());
            sumhi-=*it;
            hi.erase(it);
        }
        norm();
    }

    /* cost to make all element eq median */
    int cost() {
        int med=*(hi.begin());
        return abs(med*(int)lo.size()-sumlo)+abs(med*(int)hi.size()-sumhi);
    }
};

/* We need to move the window from 
 * left to right maintaining the sum of its elements,
 * and the (one or both) median values.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    win w;

    for(int i=0; i<k; i++)
        w.add(a[i]);

    for(int i=k; i<n; i++) {
        cout<<w.cost()<<" ";
        w.remove(a[i-k]);
        w.add(a[i]);
    }
    cout<<w.cost()<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
