
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Two trominos can build a 3x2 rectangle.
 * If we can create the whole grid from 
 * such 3x2 rects it is trivial to construct.
 * Else assume there is no solution.
 * Proved by didn't found one.
 *
 * How to find if we can fill the grid with 3x2 tiles?
 * ie 6x5 is possible, just use 6x2 and 6x3
 * ...
 * dp[i][j]=pair<pii,pii>== the two rects we can make up i,j
 * ***
 * But for some reason WA :/
 */
const int N=101;
using ppii=pair<pii,pii>;
vector<vector<ppii>> dp(N, vector<ppii>(N, {{-1,-1},{-1,-1}}));

void solve() {
    cini(n);
    cini(m);

    if(dp[n][m].first.first>=0) 
        cout<<"YES"<<endl;
    else {
        cout<<"NO"<<endl;
        return;
    }

    const vi dx={-1,1,0,0};
    const vi dy={0,0,-1,1};
    vs ans(n, string(m, 'A'-1));

    function<char(vector<pii>)> color=[&](vector<pii> cells) {
        vb vis(27);
        const char BASE='A'-1;
        for(auto [i,j] : cells) {
            for(int k=0; k<4; k++) {
                const int ii=i+dx[k];
                const int jj=j+dy[k];
                if(ii>=0 && ii<n && jj>=0 && jj<m)
                    vis[ans[ii][jj]-BASE]=true;
            }
        }
        for(size_t i=1;i<vis.size(); i++) 
            if(!vis[i])
                return (char)(BASE+i);

        assert(false);
    };

    function<void(int,int,int,int)> draw=[&](int i, int j, int isz, int jsz) {
        //cerr<<"draw, i="<<i<<" j="<<j<<" isz="<<isz<<" jsz="<<jsz<<endl;
        if(isz==2 && jsz==3) {
            char c=color({{i,j},{i,j+1},{i+1,j}});
            ans[i][j]=c;
            ans[i][j+1]=c;
            ans[i+1][j]=c;
            c=color({{i+1,j+1},{i+1,j+2},{i,j+2}});
            ans[i+1][j+1]=c;
            ans[i+1][j+2]=c;
            ans[i][j+2]=c;
        } else if(isz==3 && jsz==2) {
            char c=color({{i,j},{i,j+1},{i+1,j}});
            ans[i][j]=c;
            ans[i][j+1]=c;
            ans[i+1][j]=c;
            c=color({{i+1,j+1},{i+2,j},{i+2,j+1}});
            ans[i+1][j+1]=c;
            ans[i+2][j]=c;
            ans[i+2][j+1]=c;
        } else {
            auto [p1,p2]=dp[isz][jsz];
            //cerr<<"p1="<<p1.first<<","<<p1.second<<" p2="<<p2.first<<","<<p2.second<<endl;
            assert(p1.first>0);

            if(p1.first+p2.first==isz)
                assert(p1.second==p2.second);

            if(p1.second+p2.second==jsz)
                assert(p1.first==p2.first);

            if(p1.first+p2.first==isz) { 
                //cerr<<"split horz"<<endl;
                draw(i, j,           p1.first, p1.second);
                draw(i+p1.first, j, p2.first, p2.second);
            } else {  
                //cerr<<"split vert"<<endl;
                draw(i,          j, p1.first, p1.second);
                draw(i, j+p1.second, p2.first, p2.second);
            }
        }
    };

    draw(0,0,n,m);

    for(int i=0; i<n; i++)
        cout<<ans[i]<<endl;
}

void init() {
    dp[3][2]={{0,0},{0,0}};
    dp[2][3]={{0,0},{0,0}};
    queue<pii> q;
    q.push({3,2});
    q.push({2,3});
    while(q.size()) {
        auto [i,j]=q.front();
        q.pop();
        assert(dp[i][j].first.first>=0);

        /* same row */
        for(int ii=1; ii+i<N; ii++) {
            if(dp[ii][j].first.first>=0 && dp[ii+i][j].first.first<0) {
                dp[ii+i][j]={ {i,j}, {ii,j} };
                q.push({ii+i,j});
            }
        }
        /* same col */
        for(int jj=1; jj+j<N; jj++) {
            if(dp[i][jj].first.first>=0 && dp[i][jj+j].first.first<0) {
                dp[i][jj+j]={ {i,j}, {i,jj} };
                q.push({i,j+jj});
            }
        }
    }

    for(int i=1; i<N; i++) {
        for(int j=1;j<N; j++)  {
            if(dp[i][j].first.first>=0)
                cerr<<'1';
            else
                cerr<<'0';
        }
        cerr<<endl;
    }
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
