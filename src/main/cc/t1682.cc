
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/scc>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

inline void fs(int &number) {
#define gc() getchar_unlocked()

    int c = gc();
    while (c == ' ' || c == 13 || c == 10)
        c = gc();

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }
}

/* see http://www.zverovich.net/2013/09/07/integer-to-string-conversion-in-cplusplus.html
 *  * We could make it even faster using 3 (or 4?) digits per division. */
const char digits[] =
    "00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899";

#define pc(x) putchar_unlocked(x);
inline void writeInt(ll n) {
    char buf[32];
    int idx = 0;

    if (n == 0) {
        pc('0');
        return;
    }

    while (n >= 100) {
        ll val = n % 100;
        buf[idx++] = digits[val * 2 + 1];
        buf[idx++] = digits[val * 2];
        n /= 100;
    }

    while (n) {
        buf[idx++] = n % 10 + '0';
        n /= 10;
    }

    while (idx--) {
        pc(buf[idx]);
    }
}

/**
 * We can solve with SCC.
 * If there is more than one component, we can use first vertex (a)
 * from first component, and first from second component (b).
 *
 * If there is a path from a to b, then there is none from b to a.
 * So do scc, then dfs.
 */
void solve() {
    int n,m;
    fs(n);
    fs(m);

    scc_graph g(n);

    vvi adj(n);
    for(int i=0; i<m; i++) {
        int a,b;
        fs(a); a--;
        fs(b); b--;
        adj[a].push_back(b);
        g.add_edge(a,b);
    }

    vvi c=g.scc();

    if(c.size()==1) {
        pc('Y'); pc('E'); pc('S'); pc('\n');
    } else {
        vb vis(n);
        function<void(int)> dfs=[&](int v) {
            vis[v]=true;
            for(int chl : adj[v])
                if(!vis[chl])
                    dfs(chl);
        };
        const int a=c[0][0];
        const int b=c[1][0];

        dfs(a);
        pc('N'); pc('O'); pc('\n');
        if(vis[b]) {
            writeInt(b+1);
            pc(' ');
            writeInt(a+1);
            pc('\n');
        } else { 
            writeInt(b+1);
            pc(' ');
            writeInt(a+1);
            pc('\n');
        }
    }
}

signed main() {
    solve();
}
