
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/fenwicktree>
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

using mint=modint1000000007;

#define endl "\n"

/*
 * Compress x[]
 * Then use dp[i]=number of seqs ending in x[i]
 * ***
 * Note that if we sort x[] after compressing, we
 * can optimize to maintain the prefix sum as
 * a single value, hence do not need the fenwick.
 */
void solve() {
    cini(n);
    map<int,int> m;
    vi x(n);;
    for(int i=0; i<n; i++) {
        cin>>x[i];
        m[x[i]]=0;
    }

    int val=1;
    for(auto it=m.begin(); it!=m.end(); it++)
        it->second=val++;

    /* compress x[] */
    for(int i=0; i<n; i++)
        x[i]=m[x[i]];

    fenwick_tree<mint> fen(n+1);
    for(int i=0; i<n; i++) {
        mint cnt=1+fen.sum(0,x[i]);
        fen.add(x[i], cnt);
    }
    mint ans=fen.sum(0,n+1);
    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
