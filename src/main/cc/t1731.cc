
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Simple trie implementation.
 * Use like 'trienode root()';
 */
struct trienode {
    bool fini=false;
    trienode *chl[26]={0};

    trienode() {
    }

    /* add a string to the trie */
    void add(string &s, const int idx) {
        assert(idx<=(int)s.size());
        if(idx==(int)s.size()) {
            fini=true;
            return;
        }

        if(chl[s[idx]-'a']==0)
            chl[s[idx]-'a']=new trienode();

        chl[s[idx]-'a']->add(s,idx+1);
    };

    /* calls cb on every full word match with the index. */
    void query(string &s, const int idx, function<void(int)> cb) {
        if(fini)
            cb(idx);

        if(idx<(int)s.size() && chl[s[idx]-'a']!=0)
            chl[s[idx]-'a']->query(s, idx+1,cb);
    }
};
/**
 * We need to create a tree of the dict words, so that
 * we chain words that are prefixes to the longer words.
 *
 * Then, at each position where we need to find the matching words, 
 * we can search them in our tree, and do not have to iterate the 
 * words again and again.
 * Instead, at each position we need to iterate at most the times
 * of the longest match.
 * This results in O(n^2)
 *
 * Organize the dict words in a trie.
 */
using mint=modint1000000007;
void solve() {
    cins(s);
    cini(n);

    trienode root;

    for(int i=0; i<n; i++) {
        cins(t);
        root.add(t,0);
    }

    vector<mint> dp(s.size()+1);
    dp[0]=1;
    for(size_t i=0; i<s.size(); i++)
        if(dp[i].val()!=0)  {
            root.query(s, i, [&](int idx) { 
                    dp[idx]+=dp[i]; });
        }

    cout<<dp[s.size()].val()<<endl;
}

signed main() {
    solve();
}
