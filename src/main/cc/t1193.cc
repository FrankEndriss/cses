/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef pair<int, ll> pill;
typedef pair<ll, int> plli;
typedef pair<ll, ll> pllll;

typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<string> vs;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<pii> vpii;
typedef vector<pllll> vpllll;
typedef vector<pill> vpill;
typedef vector<plli> vplli;
typedef vector<vi> vvi;
typedef vector<vll> vvll;
typedef vector<vpii> vvpii;
typedef vector<vpllll> vvpllll;
typedef vector<vpill> vvpill;
typedef vector<vpill> vvpill;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vs ma(n);
    pii start, finish;
    fori(n) {
        cin>>ma[i];
        for(int j=0; j<m; j++) {
            if(ma[i][j]=='A')
                start={ i, j};
            else if(ma[i][j]=='B') {
                finish={ i, j};
                ma[i][j]='.';
            }
        }
    }

    vvi vis(n, vi(m));
    set<pii> next;
    next.insert(start);
    int step=0;
    while(next.size()>0 && vis[finish.first][finish.second]==0) {
        step++;
        set<pii> nextnext;
        for(pii p : next) {
            vis[p.first][p.second]=step;

            if(p.first>0 && ma[p.first-1][p.second]=='.' && vis[p.first-1][p.second]==0)
                nextnext.insert({p.first-1, p.second});
            if(p.first<n-1 && ma[p.first+1][p.second]=='.' && vis[p.first+1][p.second]==0)
                nextnext.insert({p.first+1, p.second});
            if(p.second<m-1 && ma[p.first][p.second+1]=='.' && vis[p.first][p.second+1]==0)
                nextnext.insert({p.first, p.second+1});
            if(p.second>0 && ma[p.first][p.second-1]=='.' && vis[p.first][p.second-1]==0)
                nextnext.insert({p.first, p.second-1});
        }
        swap(next, nextnext);
    }


    if(vis[finish.first][finish.second]==0) {
        cout<<"NO"<<endl;
        return 0;
    }
    cout<<"YES"<<endl;
    cout<<vis[finish.first][finish.second]-1<<endl;

    string ans;
    pii p=finish;
    while(p!=start) {
        if(p.first>0 && vis[p.first-1][p.second]==vis[p.first][p.second]-1) {
            p.first--;
            ans+="D";
        } else if(p.first<n-1 && vis[p.first+1][p.second]==vis[p.first][p.second]-1) {
            p.first++;
            ans+="U";
        } else if(p.second>0 && vis[p.first][p.second-1]==vis[p.first][p.second]-1) {
            p.second--;
            ans+="R";
        } else if(p.second<m-1 && vis[p.first][p.second+1]==vis[p.first][p.second]-1) {
            p.second++;
            ans+="L";
        } else
            assert(false);
    }
    reverse(ans.begin(), ans.end());
    cout<<ans<<endl;
}

