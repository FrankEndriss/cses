
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=50001;   /* number of primes */
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

void cdivs(ll n, vector<ll> &d) {
    if(n>1)
        d.push_back(1);

    for (ll p : pr) {
        if(p*p>n)
            break;
        if(n%p==0) {
            d.push_back(p);
            if(p*p!=n)
                d.push_back(n/p);
        }
    }
}

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

/* If the neclace would not be a circle it would be
 * n^m different combinations.
 * So we can subtract the number of cyclic shifts from 
 * that number.
 *
 * How to find the number of different cyclic shifts (p1) for all permutations?
 * There are two kinds of cyclic shifts:
 * -the ones where p1 eq p0
 * -the ones where p1 eq some other px
 *  Actually these are ignoreable, since the number is n-theFirstOnes
 *
 * Example n=4 m=3
 * 81 combinations, consider each one having 3 different rot shifts,
 * ie 4 of them are within one aequivalence class.
 *
 * But 3 of them have 0 diff rot shifts (1111, 2222, 3333),
 * ie aeq class has only one member.
 *
 * And 3*3 of them have 1 diff rot shift,
 * ie aeq class has two members
 * 
 * And 3 of these 9 are the 3 of group 1.
 *
 * 81-9 = 72 are in 4-aeq classes, 72/4=18
 *
 * 9-3 = 6 are in 2-aeq classes, 6/2=3
 *
 * 3 are in 1-aeq classes, 3
 * 18+3+3=24
 *
 * So we need to get the inclusion/exclusion right.
 * ...
 * Which turns out to be fucking complecated :/
 *
 * Actually we need to use every divisor of n...somehow.
 * Example n=12
 * ans=(m^12 - m^6 - m^4)/12 +
 *     (m^6 - m^3 - m^2)/6 +
 *     (m^4 - m^2)/4 +
 *     (m^2 - m^1)/2
 * Problem is, in first row we have subtracted the m^2 twice...so it is wrong.
 * This is, because m^6 includes the m^3 and m^2, and m^4 also includes m^2.
 *
 * m^12 - m^6 - m^4 + m^2 are in the 12-aeq class
 * ...
 * Ok, let pd[] be the prime divisors of n, then we subtract
 * all m^(n/pd[i])
 * let be phi=number of prime divisors
 * Then we removed each of them phi-1 times, so we need to add
 * ...
 *
 ****
 * see 
 * https://de.wikipedia.org/wiki/Lemma_von_Burnside
 * https://www.geeksforgeeks.org/orbit-counting-theorem-or-burnsides-lemma/
 */
void solve() {
    cini(n);
    cini(m);

    int ans=0;
    for(int i=0; i<n; i++)
        ans=pl(ans, toPower(m, gcd(i,n)));

    ans=mul(ans, inv(n));

    cout<<ans<<endl;

}

signed main() {
    solve();
}
