/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef pair<int, ll> pill;
typedef pair<ll, int> plli;
typedef pair<ll, ll> pllll;

typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<pii> vpii;
typedef vector<pllll> vpllll;
typedef vector<pill> vpill;
typedef vector<plli> vplli;
typedef vector<vi> vvi;
typedef vector<vll> vvll;
typedef vector<vpii> vvpii;
typedef vector<vpllll> vvpllll;
typedef vector<vpill> vvpill;
typedef vector<vpill> vvpill;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n, k;
    cin>>n>>k;
    vll x(n);
    ll mxx=0;
    fori(n) {
        cin>>x[i];
        mxx=max(mxx, x[i]);
    }

/* @return true if x can be devided into k or less subarrays of max sum maxSum */
    auto possible=[&](ll maxSum) {
        if(maxSum<mxx)
            return false;

        ll sum=maxSum;
        int c=0;
        fori(n) {
            if(sum+x[i]>maxSum) {
                sum=x[i];
                c++;
            } else
                sum+=x[i];

            if(c>k)
                return false;
        }
        return true;
    };

    ll l=mxx-1;
    ll r=1e16+1;
    while(l<r-1) {
        ll mid=(l+r)/2;
        if(possible(mid))
            r=mid;
        else
            l=mid;
    }
    cout<<r<<endl;
}

