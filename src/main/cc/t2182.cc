
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


constexpr int MOD=1e9+7;

inline int mul(const ll v1, const ll v2) {
    return (int)((v1*v2) % MOD);
}

inline int mul2(const ll v1, const ll v2) {
    return (int)((v1*v2) % (MOD-1));
}


int pl(int v1, int v2) {
    int res = v1 + v2;

    while(res < 0)
        res += MOD;

    while(res>=MOD)
        res-=MOD;

    return res;
}

/* int pow */
int toPower(int a, ll p) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int inv(const int x) {
    return toPower(x, MOD - 2);
}

/*
 * See https://cp-algorithms.com/algebra/divisors.html
 *
 * Product of divs:
 * product for one primefactor i is
 * x[i]*x[i]^2*x[i]^3*...*x[i]^k[i]
 * =x[i]^(k[i]*k[i]+1)/2
 *
 * Foreach primefactor there are k[i]+1 different
 * numbers.
 * 1
 * x[i]
 * x[i]^2
 * ...
 * x[i]^k[i]
 *
 * All combinations contribute, so each element of the ith primefactor
 * is part of
 * anscnt/(k[i]+1)
 * factors. Where the other primefactors of
 * that factor are all combinations of all other factors.
 * But we cannot mod the power :/
 * So we would need to execute all those powers...but
 * thats way to much operations.
 * We cannot power each x[i] with each k[j]+1
 * ...
 * We can mod the power, but we need to mod with MOD-1:
 * toPower(x,j,MOD)==toPower(x,j%(MOD-1),MOD)
 * This comes from fermats little theorem, see 
 * https://cp-algorithms.com/algebra/binary-exp.html#toc-tgt-3
 */
void solve() {
    cini(n);
    vi x(n);
    vi k(n);
    vi kk(n);

    for(int i=0; i<n; i++) {
        cin>>x[i]>>k[i];
        kk[i]=k[i]+1;
    }

    vi kL(n);
    vi kR(n);
    kL[0]=k[0]+1;
    kR[n-1]=k[n-1]+1;
    for(int i=1;i<n; i++) {
        kL[i]=mul2(kL[i-1],k[i]+1);
        kR[n-1-i]=mul2(kR[n-i],k[n-1-i]+1);
    }

    int anscnt=1;
    int anssum=1;

    for(int i=0; i<n; i++) {
        anscnt=mul(anscnt, k[i]+1);

        const int iv=inv(x[i]-1);
        anssum=mul(anssum, mul(iv, pl(toPower(x[i], k[i]+1), -1)));
    }

    int anspro=1;

    for(int i=0; i<n; i++) {
        x[i]=toPower(x[i], ((ll)k[i]*(k[i]+1)/2)%(MOD-1));
        if(i>0) 
            x[i]=toPower(x[i], kL[i-1]);
        if(i+1<n)
            x[i]=toPower(x[i], kR[i+1]);

        anspro=mul(anspro, x[i]);
    }

    cout<<anscnt<<" "<<anssum<<" "<<anspro<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
