/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*  On ith query we want to find the smallest
 *  hotel number among all hotels with more than m[i]
 *  rooms.
 *  Then update that hotel to have m[i] less rooms.
 *
 *  Since the updates we need to do this online.
 *
 *  Consider:
 *  sort hotels by number of rooms,
 *  insert ids in that order into segtree.
 *  for query 
 *    find index of min hotel in sorted list
 *    query segtree for min id right of that position
 *  update changes the number of free rooms,
 *  so the updated hotel moves left in the list.
 *  all hotels between old and new position move one position right.
 *  ...somehow :/
 */
void solve() {
    cini(n);
    cini(m);

    set<pii> s;
    for(int i=0; i<n; i++) {
        cini(h);
        s.emplace(h,i);
    }

    for(int i=0; i<m; i++) {
        cini(r);
        auto it=s.lower_bound({r,0});
        if(it==s.end()) 
            cout<<0<<endl;
        else {
            cout<<it->second+1<<endl;
            int cnt=it->first-r;
            int id=it->second;
            s.erase(it);
            s.emplace(cnt, id);
        }
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
