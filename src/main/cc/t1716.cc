
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

const int N=2e6+7;
vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

/* First child can get x=0..m apples.
 * Foreach case the other childs get m-x apples.
 * So it should be a dp.
 * But constraints are to big :/
 * So we need to find a closed formular.
 *
 * let d(n,m) the number to distrib m apples to n childs, then
 * d(n,m)=
 *  d(n-1, 0)
 * +d(n-1, 1)
 * +...
 * +d(n-1, m)
 *
 * and d(1,x)=1
 *
 * d(2,x)=
 *   d(1,0)
 *  +d(1,1)
 *  +...
 *  +d(1,x)
 *  =x+1
 * d(3,x)=
 *   d(2,0)=1
 * + d(2,1)=2
 * + ...
 * + d(2,x)=x+1
 * = (x+1)*(x+2)/2
 *
 * ...looks like the binomial coefficient.
 * 0: 1  1  1  1  1  1  1  1  1  1  1  1  1  1
 * 1: 1  2  3  4  5  6  7  8  9  10 11 12 13 14
 * 2: 1  3  6 10 15 21 28 36 ...
 * 3: 1  4 10 20 35 56 ...
 * 4: 1  5 15 35 ...
 * 5: ...
 * Watch the diagonals...
 * ans=nCr(n+m, m)
 */
void solve() {
    cini(n);
    cini(m);

    cout<<nCr(n+m-1, m)<<endl;
}

signed main() {
    solve();
}
