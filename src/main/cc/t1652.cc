/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


/* Well, this is the standard problem for a 2D fenwick tree.
 * But since there are no updates we can use more efficient
 * prefix sums 2D array.
 */
void solve() {
    cini(n);
    cini(q);

    vvi dp(n,vi(n));

    for(int i=0; i<n; i++) {
        cins(s);
        for(size_t j=0; j<s.size(); j++) {
            if(i>0)
                dp[i][j]+=dp[i-1][j];
            if(j>0)
                dp[i][j]+=dp[i][j-1];

            if(i>0 && j>0)
                dp[i][j]-=dp[i-1][j-1];

            if(s[j]=='*')
                dp[i][j]++;
        }
    }

    for(int i=0; i<q; i++) {
        cini(i1); i1--;
        cini(j1); j1--;
        cini(i2); i2--;
        cini(j2); j2--;

        int ans=dp[i2][j2];
        if(i1>0)
            ans-=dp[i1-1][j2];
        if(j1>0)
            ans-=dp[i2][j1-1];
        if(i1>0 && j1>0)
            ans+=dp[i1-1][j1-1];

        cout<<ans<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
