/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

int main() {
    ll n;
    cin>>n;

    ll ans=0;
    while(n>0) {
        ans+=n/5;
        n/=5;
    }

    cout<<ans<<endl;

}

