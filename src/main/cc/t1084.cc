/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m, k;
    cin>>n>>m>>k;
    vector<int> a(n), b(m);
    fori(n)
        cin>>a[i];
    fori(m)
        cin>>b[i];

    sort(a.begin(), a.end());
    sort(b.begin(), b.end());

    int aIdx=0; 
    int bIdx=0;

    int ans=0;
    while(aIdx<n && bIdx<m)  {
        if(abs(a[aIdx]-b[bIdx])<=k) {
            ans++;
            aIdx++;
            bIdx++;
        } else if(a[aIdx]<b[bIdx])
            aIdx++;
        else
            bIdx++;
    }

    cout<<ans<<endl;

}

