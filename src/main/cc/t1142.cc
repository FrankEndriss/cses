
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Assume to know foreach position where
 * left of it (and right of it) is the next
 * position with value smaller than current position.
 */
void solve() {
    cini(n);
    cinai(a,n);

    stack<pii> st;
    st.push({-1,-1});
    vi ri(n, n);
    for(int i=0; i<n; i++) {
        while(st.top().first>a[i]) {
            ri[st.top().second]=i;
            st.pop();
        }
        st.push({a[i],i});
    }

    while(st.size())
        st.pop();
    st.push({-1,n});
    vi le(n, -1);
    for(int i=n-1; i>=0; i--) {
        while(st.top().first>a[i]) {
            le[st.top().second]=i;
            st.pop();
        }
        st.push({a[i],i});
    }

    int ans=0;
    for(int i=0; i<n; i++) {
        assert(le[i]<i);
        assert(ri[i]>i);
        ans=max(ans, a[i]*(ri[i]-le[i]-1));
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
