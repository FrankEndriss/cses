
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(6);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vvd= vector<vd>;
using vvvd= vector<vvd>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to find foreach robot the propabilities foreach cell to end there.
 * Then, we can calc foreach cell the propability that none of the robots
 * will end there. And that is the expected value for that cell, sum of
 * all cells eq ans.
 */
void solve() {
    cini(k);

    /* dp[i][j][r]= prop of robot r to end at position i,j */
    vvvd dp(8, vvd(8, vd(64)));

    /* dirs[i][j]= number of possible moving dirs at cell i,j */
    vvi dirs(8, vi(8, 4));
    for(int i=0; i<8; i++)
        dirs[0][i]=dirs[7][i]=dirs[i][0]=dirs[i][7]=3;
    dirs[0][0]=dirs[0][7]=dirs[7][0]=dirs[7][7]=2;

    const vi dx= { -1, 0, 1,  0 };
    const vi dy= {  0, 1, 0, -1 };
    for(int r=0; r<64; r++) {
        /* p[i][j]=prop of current robot to be at i,j after current step */
        vvd p(8, vd(8));
        p[r/8][r%8]=1;

        for(int kk=0; kk<k; kk++) {
            vvd p0(8, vd(8));
            for(int i=0; i<8; i++) {
                for(int j=0; j<8; j++) {
                    for(int idx=0; idx<4; idx++) {
                        const int ii=i+dx[idx];
                        const int jj=j+dy[idx];
                        if(ii>=0 && ii<8 && jj>=0 && jj<8)
                            p0[i][j]+=p[ii][jj]/dirs[ii][jj];
                    }
                }
            }
            p.swap(p0);
        }
        for(int i=0; i<8; i++) {
            for(int j=0; j<8; j++) {
                dp[i][j][r]=p[i][j];
            }
        }
    }

    ld ans=0;
    for(int i=0; i<8; i++) {
        for(int j=0; j<8; j++) {
            ld prop=1;
            for(int kk=0; kk<64; kk++)
                prop*=(1-dp[i][j][kk]);
            ans+=prop;
        }
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
