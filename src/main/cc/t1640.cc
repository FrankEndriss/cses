/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    ll x;
    cin>>n>>x;
    vector<pair<ll, int>> a(n);
    fori(n) {
        cin>>a[i].first;
        a[i].second=i;
    }

    sort(a.begin(), a.end());
    int l=0;
    int r=n-1;
    while(l<r) {
        ll s=a[l].first+a[r].first;
        if(s==x) {
            cout<<a[l].second+1<<" "<<a[r].second+1<<endl;
            return 0;
        } else if(s<x)
            l++;
        else
            r--;
    }
    cout<<"IMPOSSIBLE"<<endl;
}

