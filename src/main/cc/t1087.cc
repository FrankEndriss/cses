/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* for prefixes of len i
 * find the one which is longest in s.
 * repeat.
 */
const char dna[]= { 'A', 'C', 'G', 'T'};

int c2i(char c) {
    for(int i=0; i<4; i++)
        if(c==dna[i])
            return i;
    assert(false);
}

void solve() {
    cins(s);
    vvi idx(4);
    for(int i=0; i<s.size(); i++)
        idx[c2i(s[i])].push_back(i);

    string ans;
    int pos=-1;
    while(true) {
        vector<pii> res;
        for(int i=0; i<4; i++) {
            auto it=upper_bound(idx[i].begin(), idx[i].end(), pos);
            if(it==idx[i].end()) {
                cout<<ans+dna[i]<<endl;
                return;
            } else {
                res.emplace_back(*it, i);
            }
        }
        pii lans=*max_element(res.begin(), res.end());
        pos=lans.first;
        ans+=dna[lans.second];
    }
    assert(false);
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

