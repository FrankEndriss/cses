/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>

#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

using namespace std;

using pii= pair<int, int>;
typedef tree<pii,null_type,less<pii>,rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;


const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 */
void solve() {
    cini(n);
    cini(q);

    indexed_set iset;
    vi p(n);
    for(int i=0; i<n; i++) {
        cin>>p[i];
        iset.insert({p[i],i});
    }

    for(int i=0; i<q; i++) {
        cins(s);
        if(s=="!") {
            cini(k);
            k--;
            cini(x);
            iset.erase({p[k], k});
            p[k]=x;
            iset.insert({p[k], k});
        } else if(s=="?") {
            cini(a);
            cini(b);
            int i1=iset.order_of_key({b+1, -1});
            int i2=iset.order_of_key({a, -1});
            cout<<i1-i2<<endl;
        } else
            assert(false);
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
