
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Since N=20 there are only 1e6 different
 * possibilities to choose some persons.
 * Each such combination has weight.
 * Use the ones with weight <=limit.
 *
 * dp[s]=<minRides,minWoflastRide>
 */
const int INF=1e9;

void solve() {
    cini(n);
    cini(x);
    cinai(w,n);
    const int N=1<<n;

    vector<pii> dp(N, { n, INF });
    dp[0]={ 1, 0 };

    for(int i=1; i<N; i++) {
        for(int j=0; j<n; j++) {
            if(i&(1<<j)) {
                pii p=dp[i^(1<<j)];
                p.second+=w[j];
                if(p.second>x) {
                    p.first++;
                    p.second=w[j];
                }
                dp[i]=min(dp[i],p); 
            }
        }
    }

    cout<<dp[N-1].first<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
