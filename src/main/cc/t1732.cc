
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* Known as the prefix function. See
 * https://cp-algorithms.com/string/prefix-function.html
 * http://algorithmsforcontests.blogspot.com/2012/08/borders-of-string.html
 * Calculate border len of s[0,n]
 * @return len of border pre/postfix in O(n) */
int calculate_border(string& s, int n) {
    int i = 1;
    int j = -1;

    vi border(n);
    border[0] = -1;
    while(i < n) {
        while(j >= 0 && s[i] != s[j+1])
            j = border[j];
        if (s[i] == s[j+1])
            j++;
        border[i++] = j;
    }
    return border.back()+1;
}


void solve() {
    cins(s);
    vi ans;
    int len=(int)s.size();
    do {
        int len1=calculate_border(s,len);
        //cerr<<"s="<<s.substr(0,len)<<" len1="<<len1<<endl;
        if(len1>0)
            ans.push_back(len1);

        if(len1>len/2) {
            int per=len-len1;   /* len of periodic string */
            len1-=per;
            while(len1>0) {
                ans.push_back(len1);
                len1-=per;
            }
            len=len1+per;
        } else
            len=len1;
    }while(len>0);

    while(ans.size()) {
        cout<<ans.back()<<" ";
        ans.pop_back();
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
