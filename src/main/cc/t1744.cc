/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int a, b;
    cin>>a>>b;
    if(a>b)
        swap(a, b);
    vector<vector<int>> dp(a+1, vector<int>(b+1));

    function<int(int, int)> rec=[&](int x, int y) {
        if(x==y)
            return 0;

        if(x>y)
            swap(x, y);

        if(dp[x][y]>0)
            return dp[x][y];
    
        int mi=10000;
        for(int i=1; i<=x/2; i++)
            mi=min(mi, rec(i, y)+rec(x-i, y)+1);
        for(int i=1; i<=y/2; i++)
            mi=min(mi, rec(x, i)+rec(x, y-i)+1);
        return dp[x][y]=mi;
    };

    cout<<rec(a, b)<<endl;

}

