/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* Range queries on big datasets in O(1) for idempotent 
 * functions like min/max.
 *
 * https://cp-algorithms.com/data_structures/sparse-table.html
 * Range queries, min/max range queries.
 * SparseTable is an immutable structure, like readonly, dont touch.
 */
const int N=2e5+7;
vi logT(N);
const bool initLog=[]() {
    logT[1]=0;
    for(int i=2; i<N; i++)
        logT[i]=logT[i/2]+1;
    return true;
}();

template<typename E>
struct SparseTable {
    int K;
    vector<vector<E>> table;
    function<E(E, E)> cum;

    template<typename Iterator>
    SparseTable(Iterator beg, Iterator end, function<E(E, E)> cummul):cum(cummul) {
        const int n=distance(beg,end);
        K=logT[n];
        table=vector<vector<E>>(n, vector<E>(K+1));

        int idx=0;
        for(; beg!=end; beg++,idx++)
           table[idx][0] = *beg;

        for (int j = 1; j <= K; j++)
            for (int i = 0; i + (1 << j) <= n; i++)
                table[i][j] = cum(table[i][j-1], table[i + (1 << (j - 1))][j - 1]);
    }

    /* range query for cummul funtions like sum or mult. O(n log n) */
    E rangeSum(int L, int R) {
        E sum = 0;
        for(int j = K; j >= 0; j--) {
            if ((1 << j) <= R - L + 1) {
                sum = cum(sum, table[L][j]);
                L += 1 << j;
            }
        }
    }

    /* range query for cummul functions like min/max. O(1) */
    E rangeMinmax(int L, int R) {
        //cerr<<"rangeMinmax, n="<<table.size()<<" L="<<L<<" R="<<R<<endl;
        int j = logT[R - L + 1];
        E ans=cum(table[L][j], table[R - (1 << j) + 1][j]);
        //cerr<<"rangeMinmax, ans="<<ans<<endl;
        return ans;
    }
};

/* The "normal" maximum subarray sum algo works with div and conq:
 * Split the whole array in two halves, then
 * ans is max of 
 * -ans for left half
 * -ans for right half
 * -max subarray sum starting from center to the left + same right
 *
 * We can extends this to find only subarrays on size a to b.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(a);
    cini(b);
    cinai(x,n);

    function<int(int,int)> go=[&](int l, int r) {
        if(r-l<a)
            return -INF;
        else if(r-l==a)
            return accumulate(x.begin()+l, x.begin()+r, 0LL);   /* better use prefix sums */

        int mid=(l+r)/2;

        vi sumL(mid-l+1);
        for(size_t idx=1; idx<sumL.size(); idx++)
            sumL[idx]=sumL[idx-1]+x[mid-idx];

        vi sumR(r-mid+1);
        for(size_t idx=1; idx<sumR.size(); idx++)
            sumR[idx]=sumR[idx-1]+x[mid+idx-1];

        /* now we need to find the max of sumL[i]+sumR[j]
         * where a<=i+j<=b
         * This is, foreach i=1..b-1 we need to find the max of range
         * sumR[max(1,a-i),...,b-i]
         */

        SparseTable<int> stR(sumR.begin(), sumR.end(), [](int i1, int i2) {
                return max(i1,i2);
        });

        int ans=max(go(l,mid), go(mid,r));

        for(int i=1; i<(int)sumL.size(); i++) {
            int rMin=max(1LL, a-i);
            int rMax=min(b-i, (int)sumR.size()-1);
            if(rMin<=rMax && rMin<(int)sumR.size())
                ans=max(ans, sumL[i]+stR.rangeMinmax(rMin, rMax));
        }

        return ans;
    };

    int ans=go(0,n);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

