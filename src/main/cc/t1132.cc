
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* This is about reroot technique.
 * Starting at some arbitray root we maintain the depth of
 * each adj node. The max dist is max of those depths.
 *
 * Then reroot to next node by calling dfs with the depth of
 * the deepest adj other than the current adj, that is, the
 * depth of the parent.
 * From the first dfs the depths of all other adj are available,
 * again max build max.
 * And so on.
 */
void solve() {
    cini(n);
    vvi adj(n);
    vvi dth(n);
    vi lvl(n);

    for(int i=0; i<n-1; i++) {
        cini(a);
        a--;
        cini(b);
        b--;
        adj[a].push_back(b);
        dth[a].push_back(-1);
        adj[b].push_back(a);
        dth[b].push_back(-1);
    }

    function<int(int,int)> dfs1=[&](int v, int p) {
        if(p>=0)
            lvl[v]=lvl[p]+1;

        int ans=1;
        for(size_t i=0; i<adj[v].size(); i++) {
            const int chl=adj[v][i];
            if(chl!=p)  {
                dth[v][i]=dfs1(chl,v);
                ans=max(ans, dth[v][i]+1);
            }
        }
        return ans;
    };

    dfs1(0, -1);

    vi ans(n);
    function<void(int,int,int)> dfs2=[&](int v, int p, int d) {
        ans[v]=d;
        /* maintain two most distant adj */
        multiset<pii> two;
        two.emplace(d,p);
        two.emplace(0,-1);
        for(size_t i=0; i<adj[v].size(); i++) {
            if(adj[v][i]==p)
                dth[v][i]=d+1;

            two.emplace(dth[v][i], adj[v][i]);
            two.erase(two.begin());

            ans[v]=max(ans[v], dth[v][i]);
        }

        auto it=two.begin();
        const pii d2=*it;
        it++;
        const pii d1=*it;
        for(size_t i=0; i<adj[v].size(); i++) {
            if(adj[v][i]!=p) {
                if(adj[v][i]!=d1.second)
                    dfs2(adj[v][i], v, d1.first);
                else
                    dfs2(adj[v][i], v, d2.first);
            }
        }
    };

    dfs2(0, -1, 0);

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}


signed main() {
    solve();
}
