/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int, int> pii;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    
    vvi tree(n+1);
    fori(m) {
        int a, b;
        cin>>a>>b;
        tree[a].push_back(b);
        tree[b].push_back(a);
    }

    vb vis(n+1);
    function<void(int)> dfs=[&](int node) {
        vis[node]=true;
        for(int c : tree[node])
            if(!vis[c])
                dfs(c);
    };
    vi leads;
    for(int i=1; i<=n; i++) {
        if(!vis[i]) {
            leads.push_back(i);
            dfs(i);
        }
    }

    cout<<leads.size()-1<<endl;
    for(int i=1; i<leads.size(); i++)
        cout<<leads[i-1]<<" "<<leads[i]<<endl;
}

