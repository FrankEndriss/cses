/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

int main() {
    string s,t;
    cin>>s>>t;

    vector<vector<int>> dp(s.size()+1, vector<int>(t.size()+1));

    for(size_t i=0; i<=s.size(); i++)
        dp[i][0]=(int)i;
    for(size_t i=0; i<=t.size(); i++)
        dp[0][i]=(int)i;

    for(size_t i=1; i<=s.size(); i++)
        for(size_t j=1; j<=t.size(); j++) {
            int cost=0;
            if(s[i-1]!=t[j-1])
                cost=1;
            dp[i][j]=min(dp[i-1][j]+1, min(dp[i][j-1]+1, dp[i-1][j-1]+cost));
        }

    cout<<dp[s.size()][t.size()];
}

