
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Maintain a list/idset of segments, foreach segment maintain  start,end
 * also maintain two sets of segment IDs, 
 * -one sorted by end,
 * -one sorted by size
 * Then each operation is one of cases:
 * -split of segment into 3 new segments
 * -change of two consecutive segments
 * -remove of size==1 segment, and merge of the two adj segments
 * Just implement that state changes, O(MlogN)
 */
void solve() {
    cini(n);
    cini(m);

    vector<pii> seg;    /* <size,end> */
    seg.emplace_back(0,0);  /* comparator object */
    seg.emplace_back(n,n);  /* initial single segment */

    struct sizecmp {
        bool operator() (const int &lhs, const int &rhs) const {
            return (seg[lhs].first==seg[rhs].first && seg[lhs].second<seg[rhs].second)
                || seg[lhs].first<seg[rhs].first;
        }
    };
    struct endcmp {
        bool operator() (const int &lhs, const int &rhs) const {
            return seg[lhs].second<seg[rhs].second;
        }
    };


    set<int,endcmp> byEnd;
    set<int,sizecmp> bySize;

    byEnd.insert(1);
    bySize.insert(1);

    for(int i=0; i<m; i++) {
        cini(x);
        seg[0].second=x;
        auto it=byEnd.lower_bound(0);
        const int id=*it;
        const int r=seg[id].second;
        const int l=r+1-seg[id].first;
        assert(l<=x && r>=x);

        if(x>l && x<r) {    /* one segment changes, two new segments */
            bySize.erase(id);
            seg[id].first=r-x;
            bSize.insert(id);
            seg.emplace_back(1,x);
            seg.emplace_back(x-l,x-1);
            bySize.insert(seg.size()-1);
            bySize.insert(seg.size()-2);
            byEnd.insert(seg.size()-1);
            byEnd.insert(seg.size()-2);
        } else if(x==l && x==r) { /* one segment removed, two segments merged into 1 */
            auto itR=it;
            itR++;
            if(itR!=byEnd.end()) {  /* merge it and itR */
                bySize.erase(id);
                bySize.erase(*itR);
                byEnd.erase(it);
                seg[*itR].first++;
                bySize.insert(*itR);
                it=itR;
            }

            if(it!=byEnd.begin()) { /* merge it and itL */
                auto itL=it;
                itL--;
                bySize.erase(*it);
                bySize.erase(*itL);
                seg[*it].first+=seg[*itL].first;
                byEnd.erase(itL);
                bySize.insert(*it);
            }

        } else if(x==l) {   /* two segments change */
            // TODO
        } else if(x==r) {   /* other tow segments change */
            // TODO
        } else 
            assert(false);  /* something wrong */

        cout<seg[*bySize.rbegin()].second<<' ';
    }
    cout<<endl;
}

signed main() {
    solve();
}
