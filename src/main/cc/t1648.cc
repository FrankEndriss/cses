/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

template <typename E>
class SegmentTree {
private:
    vector<E> tree;
    E neutral;
    function<E (E, E)> cumul;

    int treeDataOffset;
    const int ROOT = 1;

    inline int leftChild(const int treeIdx) {
        return  treeIdx * 2;
    }
    inline int rightChild(const int treeIdx) {
        return treeIdx * 2 + 1;
    }
    inline int parent(const int treeIdx) {
        return treeIdx / 2;
    }
    inline bool isOdd(const int idx) {
        return (idx & 0x1);
    }

public:
/** SegmentTree. Note that you can hold your data in your own storage and give
 * an Array of indices to a SegmentTree.
 * @param beg, end The initial data.
 * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
 * @param pCumul The cumulative function to create the "sum" of two nodes.
**/
    template<typename Iterator>
    SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E (E, E)> pCumul) {
        neutral=pNeutral;
        cumul=pCumul;
        treeDataOffset=(int)distance(beg, end);
        tree=vector<E>(treeDataOffset*2, pNeutral);

        int i=treeDataOffset;
        for (auto it=beg; it!=end; it++)
            tree[i++] = *it;

        for (int j=treeDataOffset - 1; j>=1; j--)
            tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
    }

    /** Lazy-Updates all elements in interval(idxL, idxR]. 
    * That updates the parent/interval nodes first. Which makes sense only if comp
    * can be implemented usefull. ie if pCumul is max, then we could use add here.
    * Example adding someConstant to all elements in interval.
    * updateLazyRange(4, 8, [&](int oldVal) { return oldVal+someConstant; });
    void updateLazyRange(int pIdxL, int pIdxR, function<E (E)> comp) {
        TODO   
    }
    */

    /** Updates all elements in interval(idxL, idxR]. 
    * iE add 42 to all elements from 4 to inclusive 7: 
    * update(4, 8, [](int i1) { return i1+42; });
    */
    void updateRange(int pIdxL, int pIdxR, function<E (E)> apply) {
        // Upate bottom to top
        pIdxL+=treeDataOffset;
        pIdxR+=treeDataOffset;
        for(int i=pIdxL; i<pIdxR; i++)
            tree[i]=apply(tree[i]);

        while (pIdxL != ROOT) {
            pIdxL=parent(pIdxL);
            pIdxR=max(pIdxL, parent(pIdxR-1));
            for(int i=pIdxL; i<=pIdxR; i++)
                tree[i]=cumul(tree[leftChild(i)], tree[rightChild(i)]);
        }
    }

    /** Updates the data at dataIdx to value. */
    void update(int dataIdx, E value) {
        int treeIdx = treeDataOffset + dataIdx;
        tree[treeIdx] = value;

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx);
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
        }
    }

    /** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
    E get(int pIdxL, int pIdxR) {
        int idxL = pIdxL + treeDataOffset;
        int idxR = pIdxR + treeDataOffset;
        E cum = neutral;
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL]);
                idxL++;
            }
            if (isOdd(idxR)) {
                idxR--;
                cum = cumul(cum, tree[idxR]);
            }
            idxL = parent(idxL);
            idxR = parent(idxR);
        }
        return cum;
    }

};

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, q;
    cin>>n>>q;
    vi x(n);
    fori(n)
        cin>>x[i];

    SegmentTree<ll> st(x.begin(), x.end(), 0, [](ll i1, ll i2) {
        return i1+i2;
    });
    fori(q) {
        int t, a, b;
        cin>>t>>a>>b;
        a--; b--;
        if(t==2) {
            ll ans=st.get(0, b+1)-st.get(0, a);
            cout<<ans<<endl;
        } else {
            st.update(a, b+1);
        }
    }
}

