/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define gc() getchar_unlocked()

inline void fs(int &number) {
    int c = gc();
    while (c == ' ' || c == 10)
        c = gc();

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }
}

const char digits[] =
    "00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899";

#define pc(x) putchar_unlocked(x);
inline void writeInt(int n) {
    char buf[32];
    int idx = 0;

    if (n == 0) {
        pc('0');
        return;
    }

    while (n >= 100) {
        int val = n % 100;
        buf[idx++] = digits[val * 2 + 1];
        buf[idx++] = digits[val * 2];
        n /= 100;
    }

    while (n) {
        buf[idx++] = (char)(n % 10 + '0');
        n /= 10;
    }

    while (idx--) {
        pc(buf[idx]);
    }
}

const int MOD=1e9+7;
ll mul(const long long v1, const long long v2) {
    return (v1 * v2) % MOD;
}

ll toPower(ll a, ll p) {
    ll res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int main() {
    int n;
    fs(n);
    for(int i=0; i<n; i++) {
        int a,b;
        fs(a);
        fs(b);
        writeInt((int)toPower(a, b));
        pc('\n');
    }
}

