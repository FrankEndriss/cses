
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll, ll> pllll;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<pllll,null_type,less<pllll>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef pair<int, ll> pill;
typedef pair<ll, int> plli;

typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<pii> vpii;
typedef vector<pllll> vpllll;
typedef vector<pill> vpill;
typedef vector<plli> vplli;
typedef vector<vi> vvi;
typedef vector<vll> vvll;
typedef vector<vpii> vvpii;
typedef vector<vpllll> vvpllll;
typedef vector<vpill> vvpill;
typedef vector<vpill> vvpill;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n, k;
    cin>>n>>k;
    vll x(n);
    indexed_set is;

    const int med=(k+1)/2-1;
    fori(n) {
        cin>>x[i];
        if(is.size()==k)
            is.erase(make_pair(x[i-k], i-k));
        is.insert(make_pair(x[i], i));
        if(is.size()==k)
            cout<<is.find_by_order(med)->first<<" ";
    }
    cout<<endl;

}

