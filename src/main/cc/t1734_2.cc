/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


#define lChild(node) (node*2)
#define rChild(node) (node*2+1)

struct MergeSortTree {
    vvi tree;
    int treeDataOffset;
    const int ROOT = 1;

    template<typename Iterator>
    MergeSortTree(Iterator beg, Iterator end) {
        int _n=end-beg;
        int n=1;
        while(n<_n)
            n*=2;

        treeDataOffset=n;
        tree=vvi(treeDataOffset*2);

        int i=treeDataOffset;
        for(auto it=beg; it!=end; it++)
            tree[i++].push_back(*it);

        for (int j=treeDataOffset-1; j>=1; j--) {
            merge(all(tree[lChild(j)]), all(tree[rChild(j)]), back_inserter(tree[j]));
        }
    }

    int get(int node, int sL, int sR, int l, int r, int val) {
        //cerr<<"get() node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r;
        assert(node>0);
        if(sL==l && sR==r)  {
            auto it=lower_bound(all(tree[node]), val);
            auto ans=distance(tree[node].begin(), it);
            //cerr<<" ans="<<ans<<endl;
            return ans;
        }
        //cerr<<endl;

        int mid=(sL+sR)/2;
        int ans=0;

        if(l<mid)
            ans=get(lChild(node), sL, mid, l, min(mid,r), val);

        if(r>mid)
            ans+=get(rChild(node), mid, sR, max(l,mid), r, val);

        return ans;
    }

    /* @return number of values in interval (l,r] with val<r */
    int query(int l, int r) {
        return get(1, 0, treeDataOffset, l, r, r);
    }

};

/* 
 * init O(logn):
 * We find for each r[i] the next same elemet right of it.
 * let r[i]== the min(j) of the elements a[j]==a[i] and j>i
 *
 * query(l,r]:
 * let d=number of r[k] in range (l,r] with r[k]<r
 * then ans=r-l-d
 *
 * Find d in O(log(n)^2) with merge sort ranges.
 */
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    map<int,int> pos;
    vi ra(n, n);
    for(int i=n-1; i>=0; i--) {
        auto it=pos.find(a[i]);
        if(it!=pos.end()) {
            ra[i]=it->second;
            it->second=i;
        } else
            pos[a[i]]=i;
    }
    //cerr<<"indexes r[]"<<endl;

    MergeSortTree r(all(ra));
    //cerr<<"created MST"<<endl;

    for(int i=0; i<q; i++) {
        cini(lo);
        cini(hi);
        //cerr<<"query lo="<<lo<<" hi="<<hi<<endl;
        int cnt=r.query(lo-1,hi);
        cout<<hi-lo+1-cnt<<endl;
    }
}

signed main() {
    solve();
}

