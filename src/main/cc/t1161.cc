
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Consider a set of breaking positions, and a size
 * of these segments.
 * Then we can binary search the position nearest 
 * to center, and recursivly split the stick/set at
 * that position.
 *
 * WA, guess this is basically correct but needs to be debugged?
 * We could also implemnt dp/brute force, and check if results
 * are same, or which/how they differ.
 */
void solve() {
    cini(x);
    cini(n);

    vi a(n+1);  /* split positions alias right ends in orig stick */
    int sum=0;
    for(int i=0; i<n; i++) {
        cini(aux);
        sum+=aux;
        a[i+1]=sum;
    }

    if(n==1) {
        cout<<0<<endl;
        return;
    }

    int ans=0;
    function<void(int,int)> go=[&](int l, int r) {
        if(l==r)
            return;
        const int sz=a[r]-a[l-1];
        //cerr<<"go l="<<l<<" r="<<r<<" sz="<<sz<<endl;
        ans+=sz;
        if(l+1>=r)
            return;

        const int xx=a[l-1]+sz/2;

        int it=distance(a.begin(), lower_bound(a.begin()+l, a.begin()+r, xx));
        if(it==r) { /* rightmost position */
            //cerr<<"case1"<<endl;
            go(l,r-1);
        } else if(it==l) { /* leftmost position */
            //cerr<<"case2"<<endl;
            go(l+1,r);
        } else {
            //cerr<<"case3"<<endl;
            auto itL=it-1;

            if(abs(a[it]-xx)<=abs(a[itL]-xx)) {  /* split at idxR */
                //cerr<<"split at idxR="<<idxR<<endl;
                go(it+1,r);
                go(l,it);
            } else {    /* split at idxL */
                //cerr<<"split at idxL="<<idxL<<endl;
                go(l,itL);
                go(itL+1,r);
            }
        }
    };

    go(1,n);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
