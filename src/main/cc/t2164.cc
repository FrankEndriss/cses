
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

inline void fs(int &number) {
#define gc() getchar_unlocked()

    int c = gc();
    while (c == ' ' || c == 13 || c == 10)
        c = gc();

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }
}

/* see http://www.zverovich.net/2013/09/07/integer-to-string-conversion-in-cplusplus.html
 * We could make it even faster using 3 (or 4?) digits per division. */
const char digits[] =
    "00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899";

#define pc(x) putchar_unlocked(x);
inline void writeInt(int n) {
    char buf[32];
    int idx = 0;

    if (n == 0) {
        pc('0');
        return;
    }

    while (n >= 100) {
        ll val = n % 100;
        buf[idx++] = digits[val * 2 + 1];
        buf[idx++] = digits[val * 2];
        n /= 100;
    }

    while (n) {
        buf[idx++] = n % 10 + '0';
        n /= 10;
    }

    while (idx--) {
        pc(buf[idx]);
    }
}

/*
 * We can calculate each full circle.
 * Let nn[i] be the number of children in the
 * circle just before the ith rounds first child
 * is removed.
 * So nn[0]=n
 * nn[i]=nn[i-1]-nn[i-1]/2
 * Each round makes the childs numbers be 2^i
 * numbers apart.
 * Each round starts with the first or the second
 * child available, so we maintain the 
 * number of the first child in the circle after each 
 * round.
 * Then loop max logn times to find the childs id
 * at position k.
 */
void solve() {
    int n,k;
    fs(n);
    fs(k);

    int fir=1;   /* id of first child */
    int nex=1;   /* offset of one childs id to next */
    int cnt=n;   /* number of childs in circle */
    bool firrem=false;    /* true if first child gets removed in current round. */

    while(k) {
        int nn=cnt/2;   /* number of childs getting removed in current round */
        if(firrem && cnt%2==1) 
            nn++;

        if(nn>=k) {
            if(!firrem)
                fir+=nex;   /* first removed child */

            writeInt(fir+nex*2*(k-1));
            pc('\n');
            return;
        }

        k-=nn;
        if(firrem)
            fir+=nex;

        nex*=2;
        if(cnt%2)
            firrem=!firrem;
        cnt-=nn;
    }
    assert(false);
}

signed main() {
    int t;
    fs(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
