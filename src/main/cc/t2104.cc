
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int INF=1e9;
struct state {
    int len, link;
    vector<int> next={ -INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF,-INF };
    //map<char, int> next;

    int firstpos;
};

/* see https://cp-algorithms.com/string/suffix-automaton.html */
struct SuffixAutomaton {
    int MAXLEN;
    vector<state> st;
    int sz, last;

    SuffixAutomaton(string str, int maxlen=-1) {
        MAXLEN=2*max((int)str.size(), maxlen);
        st=vector<state>(MAXLEN);
        init();
        for(char c : str) 
            extend(c);
    }

    /** init st to be the minimal suffix automaton, having only one state. */
    void init() {
        st[0].len = 0;
        st[0].link = -1;
        sz=1;
        last = 0;
    }

    /** Add a char to the automaton. */
    void extend(char c) {
        int cur = sz++;
        st[cur].len = st[last].len + 1;
        st[cur].firstpos=st[cur].len-1;
        int p = last;
        while (p != -1 && st[p].next[c-'a']==-INF) {
            st[p].next[c-'a'] = cur;
            p = st[p].link;
        }
        if (p == -1) {
            st[cur].link = 0;
        } else {
            int q = st[p].next[c-'a'];
            if (st[p].len + 1 == st[q].len) {
                st[cur].link = q;
            } else {
                int clone = sz++;
                st[clone].len = st[p].len + 1;
                st[clone].next = st[q].next;
                st[clone].link = st[q].link;
                st[clone].firstpos=st[q].firstpos;
                while (p != -1 && st[p].next[c-'a'] == q) {
                    st[p].next[c-'a'] = clone;
                    p = st[p].link;
                }
                st[q].link = st[cur].link = clone;
            }
        }
        last = cur;
    }

    /** @return len of longest prefix of p contained in s. 
     * @return node the node of the found state.
     * if len==p.size(), p is contained in s. */
    size_t prefix_len(string &p, int &node) {
        node=0;
        size_t pidx=0;
        while(pidx<p.size()) {
            int it=st[node].next[p[pidx]-'a'];
            if(it==-INF)
                return pidx;
            node=it;
            pidx++;
        }
        return pidx;
    }

    /** checks if p is contained in s */
    bool contains(string &p) {
        int aux;
        return p.size()==prefix_len(p, aux);
    }

    int firstpos(string &p) {
        int v;
        int len=prefix_len(p, v);
        if(len==(int)p.size())
            return st[v].firstpos-p.size()+1;
        else 
            return -1;
    }
};

void solve() {
    cins(s);
    SuffixAutomaton sa(s);
    cini(n);
    for(int i=0; i<n; i++) {
        cins(str);
        int ans=sa.firstpos(str);
        if(ans>=0)
            ans++;
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}
