/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef pair<ll, int> plli;
typedef pair<ll, int> plli;
typedef vector<plli> vplli;
typedef vector<vplli> vvplli;
typedef vector<bool> vb;
typedef vector<ll> vll;
typedef vector<vll> vvll;

const ll INF=1e18;

// TODO use correct algorythm, matrix
//
// TODO independent of matrix solution, copy dijkstra to lib
void dijkstra(int s, vector<ll> & d, vvplli &adj) {
    int n = (int)adj.size();
    d.assign(n, INF);
    vector<bool> u(n, false);

    d[s] = 0;
    for (int i = 0; i < n; i++) {
        int v = -1;
        for (int j = 0; j < n; j++) {
            if (!u[j] && (v == -1 || d[j] < d[v]))
                v = j;
        }

        if (d[v] == INF)
            break;

        u[v] = true;
        for (auto edge : adj[v]) {
            int to = edge.second;
            ll len = edge.first;

            if (d[v] + len < d[to]) {
                d[to] = d[v] + len;
            }
        }
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m, q;
    cin>>n>>m>>q;
    vvplli tree(n);
    vvll cost(n, vll(n, INF)); 

    fori(m) {
        int a, b;
        ll c;
        cin>>a>>b>>c;
        a--; b--;
        tree[a].push_back({c, b});
        tree[b].push_back({c, a});
    }

    for(int a=0; a<n; a++) {
        dijkstra(a, cost[a], tree);
    };

    fori(q) {
        int a, b;
        cin>>a>>b;
        a--; b--;
        if(cost[a][b]==INF)
            cost[a][b]=-1;
        cout<<cost[a][b]<<endl;
    }
}

