/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef pair<int, ll> pill;
typedef pair<ll, int> plli;
typedef pair<ll, ll> pllll;

typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<pii> vpii;
typedef vector<pllll> vpllll;
typedef vector<pill> vpill;
typedef vector<plli> vplli;
typedef vector<vi> vvi;
typedef vector<vll> vvll;
typedef vector<vpii> vvpii;
typedef vector<vpllll> vvpllll;
typedef vector<vpill> vvpill;
typedef vector<vpill> vvpill;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n;
    cin>>n;
    vi a(n);
    map<ll, int> pre;
    ll sum=0;
    pre[0]=1;
    fori(n) {
        cin>>a[i];
        sum+=a[i];
        sum%=n;
        while(sum<0)
            sum+=n;
        pre[sum]++;
    }

    sum=0;
    ll ans=0;
    fori(n) {
        pre[sum]--;
        ans+=pre[sum];
        sum+=a[i];
        sum%=n;
        while(sum<0)
            sum+=n;
    }

    cout<<ans<<endl;

}

