/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef unsigned int uint;
typedef long long ll;

int main() {
    int n, m, x;
    cin>>n>>m;
    vector<pair<int, int>> dp(n);
    cin>>x;
    if(x==0)
        dp[0]={ 1, m };
    else
        dp[0]={x, x};

    for(int i=1; i<n; i++) {
        cin>>x;
        if(x==0) {
            dp[i]={max(dp[i-1].first-1, 1), min(dp[i-1].second+1, m)};
        } else {
            dp[i]={x, x};
            for(int j=i-1; j>=0; j--) {
                if(dp[j].first<dp[j+1].first-1)
                    dp[j].first=max(1, dp[j+1].first-1);
                else
                    break;
            }
            for(int j=i-1; j>=0; j--) {
                if(dp[j].second>dp[j+1].second+1)
                    dp[j].second=min(dp[j+1].second+1, m);
                else
                    break;
            }
        }
    }

    const int MOD=1e9+7;
    vector<vector<int>> dp2(n);
    
    for(int i=dp[0].first; i<=dp[0].second; i++)
        dp2[0].push_back(1);

    for(int i=1; i<n; i++) {
        for(int j=dp[i].first; j<=dp[i].second; j++) {
            ll a=0;
            for(int k=-1; k<=1; k++) {
                if(j+k >= dp[i-1].first && j+k <=dp[i-1].second) 
                    a+=dp2[i-1][j+k-dp[i-1].first];
            }
            dp2[i].push_back((int)(a%MOD));
        }
    }
    ll ans=0;
    for(int a : dp2[n-1])
        ans=(ans+a)%MOD;
    
    cout<<ans<<endl;
}

