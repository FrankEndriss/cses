
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/maxflow>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(n);
    cini(m);
    cini(k);

    mf_graph<int> g(n+m+2);
    const int S=n+m;
    const int T=S+1;
    for(int i=0; i<n; i++)
        g.add_edge(S, i, 1);
    for(int i=0; i<m; i++) 
        g.add_edge(n+i, T, 1);

    vi a(k);
    vi b(k);
    vector<int> edg;
    for(int i=0; i<k; i++) {
        cin>>a[i]>>b[i];
        edg.push_back(g.add_edge(a[i]-1, n+b[i]-1, 1));
    }

    int ans=g.flow(S,T);
    cout<<ans<<endl;
    for(int i=0; i<k; i++) {
        auto e=g.get_edge(edg[i]);
        if(e.flow>0)
            cout<<e.from+1<<" "<<e.to+1-n<<endl;
    }

}

signed main() {
    solve();
}
