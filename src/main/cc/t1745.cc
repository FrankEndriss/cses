/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> x(n);
    int s=0;
    fori(n) {
        cin>>x[i];
        s+=x[i];
    }
    sort(x.begin(), x.end());
    vector<bool> dp(s+1, false);
    dp[0]=true;
    s=0;
    int ans=0;
    fori(n) {
        for(int j=s; j>=0; j--)
            if(dp[j]) {
                if(!dp[j+x[i]]) {
                    ans++;
                    dp[j+x[i]]=true;
                }
            }
        s+=x[i];
    }

    cout<<ans<<endl;
    for(uint i=1; i<dp.size(); i++) 
        if(dp[i])
            cout<<i<<" ";
    cout<<endl;
}

