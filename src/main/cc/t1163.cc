/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

void dump(set<pair<int, int>> &seg) {
cout<<"dump seg of size="<<seg.size()<<endl;
for(auto s : seg)
    cout<< s.first<<" "<<s.second<<endl;
}
int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int x, n;
    cin>>x>>n;
    set<int> bounds;  // (sorted) set of boundaries
    set<pair<int, int>> seg; // set of <size, leftBoundary>

    bounds.insert(0);
    bounds.insert(x);
    seg.insert({-(x), 0});

/* Loop x, 
 * -find prev and next boundary in bounds
 * -calc size as next-prev
 * -remove that segment from seg
 * -calc the two new segs, insert them into seg
 * -insert new bound into bounds
 * -print first entry in seg.
 */
    for(int i=0; i<n; i++) {
        int pos;
        cin>>pos;

        auto it=bounds.lower_bound(pos);
        int posR=*it;
        it--;
        int posL=*it;
//cout<<"pos="<<pos<<" posL="<<posL<<" posR="<<posR<<endl;

        bounds.insert(pos);

//cout<<"dump before erase"<<endl;
//dump(seg);
        const pair<int, int> r=make_pair(-(posR-posL), posL);
        seg.erase(r);
//cout<<"dump after erase"<<endl;
//dump(seg);

        seg.insert({ -(pos-posL), posL });
        seg.insert({ -(posR-pos), pos });
//cout<<"dump after insert"<<endl;
//dump(seg);

        cout<<-(*seg.begin()).first<<" ";
    }
    cout<<endl;

}

