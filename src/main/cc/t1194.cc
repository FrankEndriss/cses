/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

//#define DEBUG

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<string> ma(n);
    queue<pii> monsters;
    queue<pii> as;
    const int INF=n*m+1;
    vvi msteps(n, vi(m, INF));
    vvi asteps(n, vi(m));
    pii start;
    fori(n) {
        cin>>ma[i];
        for(int j=0; j<m; j++) 
            if(ma[i][j]=='M') {
                monsters.push({ i, j});
                msteps[i][j]=1;
                ma[i][j]='.';
            } else if(ma[i][j]=='A') {
                as.push({ i, j});
                asteps[i][j]=1;
                ma[i][j]='.';
            }
    }

    while(monsters.size()) {
        pii p=monsters.front();
        monsters.pop();
        int lstep=msteps[p.first][p.second]+1;
#ifdef DEBUG
cout<<"monsters, p.f="<<p.first<<" p.s="<<p.second<<" step="<<lstep<<endl;
#endif
        pii pp[]= { { p.first-1, p.second}, { p.first+1, p.second },
                    { p.first, p.second-1}, { p.first, p.second+1 }};

        for(auto p2 : pp) {
            if(p2.first>=0 && p2.first<n && p2.second>=0 && p2.second<m
                    && ma[p2.first][p2.second]=='.' 
                    && msteps[p2.first][p2.second]==INF) {
                msteps[p2.first][p2.second]=lstep;
                monsters.push({p2.first, p2.second});
            }
        }
    }

    bool fini=false;
    pii last;
    while(as.size()) {
        pii p=as.front();
        if(p.first==0 || p.first==n-1 || p.second==0 || p.second==m-1) {
            last=p;
            fini=true;
            break;
        }
        as.pop();
        int lstep=asteps[p.first][p.second]+1;
#ifdef DEBUG
cout<<"as, p.f="<<p.first<<" p.s="<<p.second<<" step="<<lstep<<endl;
#endif

        pii pp[]= { { p.first-1, p.second}, { p.first+1, p.second },
                    { p.first, p.second-1}, { p.first, p.second+1 }};
        for(auto p2 : pp) {
            if(p2.first>=0 && p2.first<n && p2.second>=0 && p2.second<m
                    && ma[p2.first][p2.second]=='.' && msteps[p2.first][p2.second]>lstep
                    && asteps[p2.first][p2.second]==0) {
                asteps[p2.first][p2.second]=lstep;
                as.push({p2.first, p2.second});
            }
        }
    }

    if(!fini) {
        cout<<"NO"<<endl;
        return 0;
    }

#ifdef DEBUG
cout<<"last.first="<<last.first<<" last.second="<<last.second<<endl;
#endif
    // backtrack last
    int lstep=asteps[last.first][last.second];
    string path;
    while(lstep>1) {
        string s[] { "D", "U", "R", "L" };
        pii pp[]= { { last.first-1, last.second}, { last.first+1, last.second },
                    { last.first, last.second-1}, { last.first, last.second+1 }};
        
#ifdef DEBUG
cout<<"backtrack, lstep="<<lstep<<endl;
#endif
        int i=0;
        bool ok=false;
        for(auto p2 : pp) {
#ifdef DEBUG
cout<<"check, p2.first="<<p2.first<<" p2.second="<<p2.second<<endl;
if(p2.first>=0 && p2.first<n && p2.second>=0 && p2.second<m)
cout<<"asteps[p2.first][p2.second]="<<asteps[p2.first][p2.second]<<endl;
#endif
            if(p2.first>=0 && p2.first<n && p2.second>=0 && p2.second<m
                    && asteps[p2.first][p2.second]==lstep-1) {
                last=p2;
                lstep--;
                path+=s[i];
                ok=true;
                break;
            }
            i++;
        }
        assert(ok);
    }

    reverse(path.begin(), path.end());
    cout<<"YES"<<endl;
    cout<<path.size()<<endl;
    cout<<path<<endl;
}

