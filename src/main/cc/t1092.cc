
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n;
    cin>>n;

    ll x=n*(n+1)/2;
    if(x%2==1) {
        cout<<"NO"<<endl;
        return 0;
    }
    x/=2;

    ll sum=0;
    ll k=n;
    vector<ll> ans;
    while(sum+k<x && k>0) {
        sum+=k;
        ans.push_back(k);
        k--;
    }

    if(sum+k>=x && sum!=x)
        ans.push_back(x-sum);
    else {  
        cout<<"NO"<<endl;
        return 0;
    }


    cout<<"YES"<<endl;
    reverse(ans.begin(), ans.end());

    vector<ll> ans2[2];
    auto it=ans.begin();
    for(int i=1; i<=n; i++) {
        if(it!=ans.end() && i<*it) {
            ans2[0].push_back(i);
        } else {
            ans2[1].push_back(i);
            if(it!=ans.end())
                it++;
        }
    }

    for(int i=0; i<2; i++) {
        cout<<ans2[i].size()<<endl;
        for(ll c : ans2[i]) 
            cout<<c<<" ";
        cout<<endl;
    }
}

