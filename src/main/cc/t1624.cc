/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cinas(s,8);

    function<int(vi&)> pos=[&](vi &pre) {
        if(pre.size()==8)
            return 1;

        int ans=0;
        for(int i=0; i<8; i++) {
            if(s[pre.size()][i]=='*')
                continue;

            bool ok=true;
            for(int j=0; j<pre.size() && ok; j++) {
                int rdist=pre.size()-j;
                if(pre[j]==i || pre[j]-rdist==i || pre[j]+rdist==i)
                    ok=false;
            }
            if(ok) {
                pre.push_back(i);
                ans+=pos(pre);
                pre.pop_back();
            }
        }
        return ans;
    };

    vi pre;
    cout<<pos(pre)<<endl;
}

