
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Let so(b)= solution for a==0, then
 * ans=so(b)-so(a-1)
 */
int so(int xb) {
    if(xb<0)
        return 0;
    else if(xb==0)
        return 1;

    vi b;
    while(xb) {
        b.push_back(xb%10);
        xb/=10;
    }
    reverse(all(b));

    vvvi dp(10, vvi(2, vi(b.size()))); /* dp[i][j][k]= number of numbers 
                                          ending in digit i, being lt b (j=0),
                                          eq b(j=1), after position k. */
    for(int i=1; i<b[0]; i++) 
        dp[i][0][0]=1;
    dp[b[0]][1][0]=1;

    for(size_t k=1; k<b.size(); k++) {  /* pos */
        for(int i=0; i<10; i++) {       /* prev digit */
            for(int j=0; j<10; j++) {   /* next digit */
                if(i==0 && j>0)
                    dp[j][0][k]++;  /* a number starting  with j */

                if(i==j)
                    continue;

                if(j==b[k])
                    dp[j][1][k]+=dp[i][1][k-1];

                dp[j][0][k]+=dp[i][0][k-1];
                if(j<b[k])
                    dp[j][0][k]+=dp[i][1][k-1];
            }
        }
    }

    int ans=0;
    for(int i=0;i<10; i++) {
        ans+=dp[i][0].back();
        ans+=dp[i][1].back();
    }
    return ans+1;
}

void solve() {
    cini(xa);
    cini(xb);

    int ans=so(xb)-so(xa-1);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
