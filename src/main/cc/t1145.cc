/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

/* see
 * https://www.geeksforgeeks.org/longest-monotonically-increasing-subsequence-size-n-log-n/
 */
int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<ll> dp;

    fori(n) {
        ll x;
        cin>>x;

        if(dp.size()==0 || x>dp.back()) // case 1 and 2
            dp.push_back(x);
        else if(x<=dp[0]) // case 1
            dp[0]=x;
        else {  // case 3
            // find first element >= x
            *lower_bound(dp.begin(), dp.end(), x)=x;
/*
            if(m!=dp.end())
                *m=x;

            for(int j=dp.size()-1; j>0; j--) {
                if(dp[j-1]<x) {
                    dp[j]=x;
                    break;
                }
            }
*/
        }
    }
    cout<<dp.size()<<endl;

}

