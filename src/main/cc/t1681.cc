/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

vector<bool> visited;
vector<int> ans;

void dfs(vvi& adj, int v) {
    visited[v] = true;
    for (int u : adj[v]) {
        if (!visited[u])
            dfs(adj, u);
    }
    ans.push_back(v);
}

vi topological_sort(vvi& adj, int n) {
    visited.assign(n, false);
    ans.clear();
    for (int i = 0; i < n; ++i) {
        if (!visited[i])
            dfs(adj, i);
    }
    reverse(ans.begin(), ans.end());
    return ans;
}

/* Number of paths in a directed graph without cycles.
 * We find a topologial sort of the vertex, then use
 * dynamic programming to count the number of paths
 * from topogical first to last.
 */
const int MOD=1e9+7;
void solve() {
    cini(n);
    cini(m);
    vvi adj(n);
    vvi adjR(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
    }

    vi topo=topological_sort(adj, n);

    vi dp(n,0);
    dp[0]=1;
    for(int v : topo) {
        for(int c : adj[v])  {
            dp[c]+=dp[v];
            dp[c]%=MOD;
        }
    }

    cout<<dp[n-1]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
