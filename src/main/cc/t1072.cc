/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n;
    cin>>n;

    //int v={ 0, 0, 6, 28, 96, 252, 550, 1056, 1848 };
    int v[]= { 0, 0, 6, 28, 96, 252 };

    for(int i=1; i<=n; i++) {
        if(i<6) {
            cout<<v[i]<<endl;
            continue;
        }

        ll f=i*i;
        ll ans=(1LL* 4*(f-2-1) +  // ecken
               8LL*(f-3-1) +       // felder neben ecken
               (i-4)*4LL*(f-4-1) + // restliche randfelder
               4LL*(f-4-1) +       // ecken 2te reihe
               (i-4)*4LL*(f-6-1) + // zweite reihe ohne ecken 2te reihe
               (i-4LL)*(i-4)*(f-8-1))  // alle anderen felder
                / 2;

        cout<<ans<<endl;
    }
}

