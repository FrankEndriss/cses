/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<pair<int, int>> evt;
    fori(n) {
        int a, b;
        cin>>a>>b;

        evt.push_back({a, 1});
        evt.push_back({b, -1});
    }

    sort(evt.begin(), evt.end());
    int mx=0;
    int cnt=0;
    for(auto p : evt) {
        cnt+=p.second;
        mx=max(mx, cnt);
    }

    cout<<mx<<endl;
}

