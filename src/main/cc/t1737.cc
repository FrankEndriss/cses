
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

struct node {
    node *left;
    node *right;
    int val;

    bool ownL=false;
    bool ownR=false;

    node() {
        left=0;
        right=0;
        val=0;
    }

    int query(int sL, int sR, int l, int r) {
        if(l>sR || r<sL)
            return 0;
        //cerr<<"query, sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<endl;

        if(l<=sL && r>=sR)
            return val;

        int mid=(sL+sR)/2;
        return left->query(sL,mid,l,r) + right->query(mid+1,sR,l,r);
    }

    void set(int sL, int sR, int idx, int v) {
        //cerr<<"set, sL="<<sL<<" sR="<<sR<<" idx="<<idx<<" v="<<v<<endl;
        if(sL==sR) {
            assert(sL==idx);
            val=v;
            return;
        }
        assert(idx>=sL && idx<=sR);

        int  mid=(sL+sR)/2;
        if(idx<=mid) {
            if(!ownL) {
                node *nleft=new node();
                nleft->left=left->left;
                nleft->right=left->right;
                nleft->val=left->val;
                ownL=true;
                left=nleft;
            }
            left->set(sL,mid,idx,v);

        } else {
            if(!ownR) {
                node *nright=new node();
                nright->left=right->left;
                nright->right=right->right;
                nright->val=right->val;
                ownR=true;
                right=nright;
            }
            right->set(mid+1,sR,idx,v);
        }
        val=left->val + right->val;
    }
};


/* Persistent segment tree
 */
void solve() {
    cini(_n);
    cini(q);

    int n=1;
    while(n<_n)
        n*=2;

    vector<node*> space(2*n);
    for(int i=1; i<2*n; i++)
        space[i]=new node();

    for(int i=1; i<n; i++) {
        space[i]->left=space[i*2];
        space[i]->right=space[i*2+1];
        space[i]->ownL=true;
        space[i]->ownR=true;
    }
    for(int i=0; i<_n; i++) {
        cini(a);
        space[i+n]->val=a;
    }

    for(int i=n-1; i>=1; i--)
        space[i]->val=space[i]->left->val + space[i]->right->val;

    //cerr<<"inited"<<endl;

    vector<node*> roots;
    roots.push_back(space[1]);
    vector<int> kmap(q);
    kmap[1]=0;
    int kk=1;

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(k);
            cini(idx);
            cini(v);
            //cerr<<"t=1; k="<<k<<" idx="<<idx<<" v="<<v<<endl;
            roots[kmap[k]]->set(0, n-1, idx-1, v);
        } else if(t==2) {
            cini(k);
            cini(l);
            cini(r);
            //cerr<<"t=2; k="<<k<<" l="<<l<<" r="<<r<<endl;
            cout<<roots[kmap[k]]->query(0, n-1, l-1, r-1)<<endl;
        } else if(t==3) {
            cini(k);
            //cerr<<"t=3; k="<<k<<endl;
            node *nnode1=new node();
            node *nnode2=new node();
            nnode1->left=roots[kmap[k]]->left;
            nnode2->left=roots[kmap[k]]->left;
            nnode1->right=roots[kmap[k]]->right;
            nnode2->right=roots[kmap[k]]->right;
            nnode1->val=roots[kmap[k]]->val;
            nnode2->val=roots[kmap[k]]->val;

            roots.push_back(nnode1);
            kk++;
            kmap[kk]=roots.size()-1;
            roots.push_back(nnode2);
            kmap[k]=roots.size()-1;

        }
    }


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
