
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider binary search.
 *
 * To check the number of smaller vals to a value x,
 * -find number of cols c0 where n*c0<=x, ans+=c0*n-c0*c0
 * -find number of cols c1 where (n-1)*c1<=x, ans+=(c1-c0)*(n-1)-(c1-c0)*(c1-c0)
 * -...this should be no more than sqrt(n) loops
 *
 * So complexity is O(sqrt(n)*logn)
 */
void solve() {
    for(int i=3; i<17; i+=2) {
        cout<<"i="<<i<<endl;
        vi d;
        for(int j=1; j<=i; j++) {
            for(int k=1; k<=i; k++) {
                if(j*k<10)
                    cout<<"00";
                else if(j*k<100)
                    cout<<"0";
                cout<<j*k<<" ";
                d.push_back(j*k);
            }
            cout<<endl;
        }
        sort(all(d));
        cout<<"m="<<d[i*i/2]<<endl;
    }
}

signed main() {
    solve();
}
