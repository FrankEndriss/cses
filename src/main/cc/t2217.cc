
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Each time a numbers position is left of
 * the number+1 position ans is increased by 1.
 *
 * Consider all pairs pos[a[i]],pos[a[i]+1], if increasing
 * or not.
 *
 * So, if we change the position of two numbers at position i and j,
 * we propably change the order in 4 of such pairs.
 * pos[a[i]-1],pos[a[i]]
 * pos[a[i]],pos[a[i]+1]
 * pos[a[j]-1],pos[a[j]]
 * pos[a[j]],pos[a[j]+1]
 * Add these changes to ans and do the swap.
 * Take care of edgecases where i or j is first or last number.
 *
 * So it is about 6 numbers and their position before and
 * after the swap.
 */
void solve() {
    cini(n);
    cini(m);

    vi a(n+1);
    vi pos(n+2);
    for(int i=1; i<=n; i++) {
        cin>>a[i];
        pos[a[i]]=i;
    }
    pos[0]=0;
    pos[n+1]=n+1;

    int ans=1;
    for(int i=1; i+1<=n; i++)
        if(pos[i]>pos[i+1])
            ans++;

    //cerr<<"initial ans="<<ans<<endl;

    for(int i=0; i<m; i++) {
        cini(p1);
        cini(p2);

        if(a[p1]>a[p2])
            swap(p1,p2);

        const vvi snum= {
            {a[p1]-1, a[p1], a[p1]+1},
            {a[p2]-1, a[p2], a[p2]+1}
        };
        /*
            cerr<<"snum="<<endl;
            for(int i=0; i<2; i++) {
                for(int j=0; j<3; j++) 
                    cerr<<snum[i][j]<<" ";
                cerr<<endl;
            }
            */

        vvi spos0(2, vi(3));
        for(int j=0; j<2; j++)
            for(int k=0; k<3; k++)
                spos0[j][k]=pos[snum[j][k]];

        swap(a[p1], a[p2]);
        pos[a[p1]]=p1;
        pos[a[p2]]=p2;

        vvi spos1(2, vi(3));
        for(int j=0; j<2; j++)
            for(int k=0; k<3; k++)
                spos1[j][k]=pos[snum[j][k]];

        if(abs(a[p1]-a[p2])>1) {
            /* Now check how the 4 pairs have changed
             **/
            for(int j=0; j<2; j++) {
                if(spos0[j][0]<spos0[j][1] && spos1[j][0]>spos1[j][1])
                    ans++;
                else if(spos0[j][0]>spos0[j][1] && spos1[j][0]<spos1[j][1])
                    ans--;

                if(spos0[j][1]<spos0[j][2] && spos1[j][1]>spos1[j][2])
                    ans++;
                else if(spos0[j][1]>spos0[j][2] && spos1[j][1]<spos1[j][2])
                    ans--;
            }
        } else {
            /*
            cerr<<"case 2"<<endl;
            cerr<<" lt="<<snum[0][1]<<" bg="<<snum[1][1]<<endl;
            cerr<<"bef pos[lt]="<<spos0[0][1]<<" pos[bg]="<<spos0[1][1]<<endl;
            cerr<<"aft pos[lt]="<<spos1[0][1]<<" pos[bg]="<<spos1[1][1]<<endl;
            cerr<<"spos0="<<endl;
            for(int i=0; i<2; i++) {
                for(int j=0; j<3; j++) 
                    cerr<<spos0[i][j]<<" ";
                cerr<<endl;
            }
            cerr<<"spos1="<<endl;
            for(int i=0; i<2; i++) {
                for(int j=0; j<3; j++) 
                    cerr<<spos1[i][j]<<" ";
                cerr<<endl;
            }
            */
            /* a[p1] and a[p2] are consecutive numbers, so not more
                            than 3 pairs can have changed 
            */
            /* pair lt-1,lt */
            if(spos0[0][0]<spos0[0][1] && spos1[0][0]>spos1[0][1])  {
                //cerr<<"case 2 1++"<<endl;
                ans++;
            } else if(spos0[0][0]>spos0[0][1] && spos1[0][0]<spos1[0][1])  {
                //cerr<<"case 2 1--"<<endl;
                ans--;
            }
            /* pair lt,bg */
            if(spos0[0][1]<spos0[0][2] && spos1[0][1]>spos1[0][2])  {
                //cerr<<"case 2 2++"<<endl;
                ans++;
            } else if(spos0[0][1]>spos0[0][2] && spos1[0][1]<spos1[0][2])  {
                //cerr<<"case 2 2--"<<endl;
                ans--;
            }
            /* pair bg,bg+1 */
            if(spos0[1][1]<spos0[1][2] && spos1[1][1]>spos1[1][2])  {
                //cerr<<"case 2 3++"<<endl;
                ans++;
            } else if(spos0[1][1]>spos0[1][2] && spos1[1][1]<spos1[1][2])  {
                //cerr<<"case 2 3--"<<endl;
                ans--;
            }
        }

        cout<<ans<<endl;

    }


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
