
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * see https://cp-algorithms.com/graph/topological-sort.html
 */

vector<bool> visited;
vector<int> ans;

void dfs(vvi& adj, int v) {
    visited[v] = true;
    for (int u : adj[v]) {
        if (!visited[u])
            dfs(adj,u);
    }
    ans.push_back(v);
}

vi topological_sort(vvi& adj, int n) {
    visited.assign(n, false);
    ans.clear();
    for (int i = 0; i < n; ++i) {
        if (!visited[i])
            dfs(adj, i);
    }
    reverse(ans.begin(), ans.end());
    return ans;
}

/* Longest path.
 * Should work with dijkstra, too.
 * -> turns out it does not.
 *
 * Topological sort
 * Then set the max path sizes to all childs
 * in topological order.
 */
void solve() {
    cini(n);
    cini(m);
    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
    }

    vi topo=topological_sort(adj, n);
    vi dp(n);
    dp[0]=1;
    vi p(n,-1);
    for(int v : topo) {
        if(dp[v]==0)
            continue;

        for(int c : adj[v])  {
            if(dp[c]<dp[v]+1) {
                dp[c]=dp[v]+1;
                p[c]=v;
            }
        }
    }
    if(dp[n-1]==0)
        cout<<"IMPOSSIBLE"<<endl;
    else {
        vi ans;
        int v=n-1;
        while(v!=0) {
            ans.push_back(v);
            v=p[v];
        } 
        ans.push_back(0);
        reverse(all(ans));
        cout<<dp[n-1]<<endl;
        for(int v : ans)
            cout<<v+1<<" ";
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
