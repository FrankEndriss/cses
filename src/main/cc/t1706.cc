
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * bitset based knapsack
 *
 * Use the 3K trick to minimize number of items.
 */
const int N=1e5+3;
void solve() {
    cini(n);
    cini(m);

    dsu d(n);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        d.merge(a,b);
    }

    vi f(n+2);
    for(size_t i=0; i<n; i++) 
        if(d.leader(i)==i) 
            f[d.size(i)]++;

    for(int i=1; i<=n; i++)  {
        if(f[i]>2) {
            const int cnt=f[i]/3;
            f[min(n+1,i*2)]+=cnt;
            f[i]-=cnt*2;
        }
    }

    bitset<N> b;
    b[0]=1;
    for(int i=0; i<=n; i++) {
        for(int j=0; j<f[i]; j++)
            b|=(b<<i);
    }

    for(int i=1; i<=n; i++) 
        cout<<b[i];
    cout<<endl;

}

signed main() {
    solve();
}
