/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    string s;
    cin>>s;
    vector<int> v;
    for(int i=0; i<s.size(); i++) {
        v.push_back(i);
    }

    set<string> ans;
    do {
        string stmp;
        for(auto c : v)
            stmp.append(1, s[c]);
        ans.insert(stmp);
    }while(next_permutation(v.begin(), v.end()));

    cout<<ans.size()<<endl;
    for(auto it=ans.begin(); it!=ans.end(); it++)
        cout<<*it<<endl;;

}

