
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int third(int i1, int i2) {
    if(i1!=1 && i2!=1)
        return 1;
    else if(i1!=2 && i2!=2)
        return 2;
    else
        return 3;
}

/* To move i discs from 0 to 1
 * we need to move i-1 discs from 0 to 2, 
 * then 1 disc from 0 to 1, then i-1 discs
 * from 2 to 1.
 */
void solve() {
    vector<pii> ans;
    function<void(int,int,int)> go=[&](int from, int to, int cnt) {
        if(cnt>1) {
            int t=third(from,to);
            go(from, t, cnt-1);
            ans.emplace_back(from, to);
            go(t, to, cnt-1);
        } else {
            ans.emplace_back(from, to);
        }
    };

    cini(n);
    go(1,3,n);

    cout<<ans.size()<<endl;
    for(pii p : ans)
        cout<<p.first<<" "<<p.second<<endl;


}

signed main() {
    solve();
}
