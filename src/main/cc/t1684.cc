
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/twosat>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

#define gc() getchar_unlocked()
inline char fc() {
    int c = gc();
    while (c == ' ' || c == 13 || c == 10)
        c = gc();

    return (char)c;
}

inline void fs(int &number) {
    int c = gc();
    while (c == ' ' || c == 13 || c == 10)
        c = gc();

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }
}

/**
 * 2SAT
 * Each topping is one variable.
 */
void solve() {
    int n,m;
    fs(n);
    fs(m);

    two_sat ts(m);

    for(int i=0; i<n; i++) {
        char c1,c2;
        int x1,x2;
        c1=fc();
        fs(x1);
        x1--;
        c2=fc();
        fs(x2);
        x2--;

        ts.add_clause(x1, c1=='+', x2, c2=='+');
    }

    if(!ts.satisfiable())  {
        cout<<"IMPOSSIBLE"<<endl;
    } else {
        vb ans=ts.answer();
        for(int i=0; i<m; i++)
            if(ans[i])
                cout<<"+ ";
            else
                cout<<"- ";
        cout<<endl;
    }


}

signed main() {
    solve();
}
