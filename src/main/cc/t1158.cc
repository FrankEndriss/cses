/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, x;
    cin>>n>>x;
    vector<int> h(n);
    vector<int> s(n);
    fori(n)
        cin>>h[i];
    fori(n)
        cin>>s[i];

    vector<int> dp(x+1); // dp[i]=max pages we can by for i coins so far.

    for(int i=0; i<n; i++) {
        for(int j=x; j>=h[i]; j--) {
            dp[j]=max(dp[j], dp[j-h[i]]+s[i]);
        }
    }
#ifdef DEBUG
cout<<"dp=";
for(int i=0; i<=x; i++)
    cout<<dp[i]<<" ";
cout<<endl;
#endif

    cout<<dp[x]<<endl;

}

