/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/*
 * Construct all 88418 paths, but in any step check
 * if that step is constrained by the input string.
 * If yes, go only the possible way.
 *
 * How to find all paths?
 * Note that when touching a border, next step must go
 * down (for right border) or left (for bottom border).
 * So right col must be visited top to bottom, bottom
 * row right to left.
 * Any more quick checkable constraints?
 *
 * TODO more optimizations...
 * see https://github.com/pllk/cphb/blob/master/book.pdf page 51ff
 */
int main() {
    cins(s);

/* optimazation:
 * if we step on brow/col, all other cell above/right
 * of that cell must have been visited, else no path.
 */
    int brow=6;
    int bcol=6;

    const vector<char> offC={ 'U', 'D', 'L', 'R'};
    const vector<pii> offs= { { -1, 0}, { 1,0}, {0,-1}, {0,1}};
    function<int(pii,vvb&,int)> paths=[&](pii pos, vvb &dp,int lvl) {
        if(pos.first==6 && pos.second==0) {
            if(lvl==49)
                return 1;
            else
                return 0;
        }

        int ans=0;
        for(int i=0; i<4; i++) {
            if(s[lvl-1]!='?' && s[lvl-1]!=offC[i])
                continue;

            if(pos.first==brow) { // bottom row
                if(offC[i]=='R')
                    continue;

                bool ok=true;
                for(int i=pos.second+1; i<7; i++) {
                    if(!dp[pos.first][i])
                        ok=false;
                }
                if(!ok)
                    continue;
            }

            if(pos.second==bcol) { // right col
                if(offC[i]=='U')
                    continue;
    
                bool ok=true;
                for(int i=pos.first-1; i>=0; i--) {
                    if(!dp[i][pos.second])
                        ok=false;
                }
                if(!ok)
                    continue;
            }

            pii npos={ pos.first+offs[i].first, pos.second+offs[i].second};

            if(npos.first<0 || npos.first>=7 || npos.second<0 || npos.second>=7)
                continue;

            if(dp[npos.first][npos.second])
                continue;

            dp[npos.first][npos.second]=true;
            ans+=paths(npos, dp, lvl+1);
            dp[npos.first][npos.second]=false;
        }
        return ans;
    };

    vvb dp(7, vb(7));
    dp[0][0]=true;
    cout<<paths({0,0},dp,1)<<endl;
}

