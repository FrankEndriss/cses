/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef pair<int, ll> pill;
typedef pair<ll, int> plli;
typedef pair<ll, ll> pllll;

typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<string> vs;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<pii> vpii;
typedef vector<pllll> vpllll;
typedef vector<pill> vpill;
typedef vector<plli> vplli;
typedef vector<vi> vvi;
typedef vector<vll> vvll;
typedef vector<vpii> vvpii;
typedef vector<vpllll> vvpllll;
typedef vector<vpill> vvpill;
typedef vector<vpill> vvpill;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vs ma(n);
    fori(n)
        cin>>ma[i];

    vvb vis(n, vb(m));

    function<void(int, int)> dfs=[&](int row, int col) {
        if(vis[row][col])
            return;

        vis[row][col]=true;
        if(row>0 && !vis[row-1][col] && ma[row-1][col]=='.')
            dfs(row-1, col);
        if(row<n-1 && !vis[row+1][col] && ma[row+1][col]=='.')
            dfs(row+1, col);
        if(col>0 && !vis[row][col-1] && ma[row][col-1]=='.')
            dfs(row, col-1);
        if(col<m-1 && !vis[row][col+1] && ma[row][col+1]=='.')
            dfs(row, col+1);
    };

    int ans=0;
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            if(ma[i][j]=='.' && !vis[i][j]) {
                ans++;
                dfs(i, j);
            }

    cout<<ans<<endl;
}

