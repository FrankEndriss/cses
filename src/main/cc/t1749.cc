
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
#include <ext/rope>

using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


using namespace __gnu_cxx;

/***************
use like

    indexed_set is;
    is.insert(41);
    is.insert(42);

    auto x=*is.find_by_order(1); // random access
    cout<<x<<endl;   // 42
    cout<<is.order_of_key(41)<<endl;   // 0


    // If the element does not appear in the set, we get the position that the element
    // would have in the set:
    cout<<is.order_of_key(43)<<endl;   // 2
    

    // https://en.wikipedia.org/wiki/Rope_(data_structure)
    rope<int> r;
    r.insert(r.mutable_begin(), 42);
    r.insert(r.mutable_begin()+1, 43);
    r.insert(r.mutable_begin(), 50);
    for(int i=0; i<3; i++)
        cout<<*(r.mutable_begin()+i)<<" "; // 50 42 43


**********************
Note we can use a lazy segment tree to store the
indexes of the positions.
Then on each removal of an element we decrement all
indexes gt that removed element.
Then the value at each position refers to the position
of that element in the (virtual) updated list.
*/
void solve() {
    cini(n);
    rope<int> r;
    for(int i=0; i<n; i++) {
        cini(x);
        r.insert(r.mutable_end(), x);
    }
    for(int i=0; i<n; i++) {
        cini(p);
        auto it=r.mutable_begin()+(p-1);
        cout<<*it<<" ";
        r.erase(it);
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
