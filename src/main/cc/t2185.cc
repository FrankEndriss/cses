
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



typedef __int128 lll;

/* We need to calc the number of multiples
 * of some primes in the range.
 * Inclusion/Exclusion.
 * So count all n/p[i]
 * then subtract all n/(p[i]*p[j])
 * then add all n/(p[i]*p[j]*p[k])
 * ...
 * until p[0]*p[1]*...*p[i]>n
 *
 * ***
 * Since K<=20 we can check all masks, that is simpler to implement.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(p, k);
    sort(all(p), greater<int>());

    int ans=0;
    for(int i=1; i<(1<<k); i++) {
        ld sum=1;
        int cnt=0;
        for(int j=0; sum<=n && j<k; j++) {
            if(i&(1<<j)) {
                sum*=p[j];
                cnt++;
            }
        }
        if(sum<=n) {
            ll lsum=sum;
            if(cnt&1)
                ans+=n/lsum;
            else
                ans-=n/lsum;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
