
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vvi tree(n+1);
    fori(m) {
        int a, b;
        cin>>a>>b;
        tree[a].push_back(b);
        tree[b].push_back(a);
    }

    vector<int> pathlen(n+1);
    queue<int> q;
    q.push(1);
    pathlen[1]=1;
    while(q.size()>0) {
        int node=q.front();       
        if(node==n)
            break;
        q.pop();
        for(int c : tree[node])
            if(pathlen[c]==0) {
                q.push(c);
                pathlen[c]=pathlen[node]+1;
            }
    }

    if(pathlen[n]==0) {
        cout<<"IMPOSSIBLE"<<endl;
        return 0;
    }

    cout<<pathlen[n]<<endl;
    int node=n;
    vi path;
    path.push_back(node);
    while(node!=1) {
        for(int c : tree[node]) {
            if(pathlen[node]==pathlen[c]+1) {
                path.push_back(c);
                node=c;
                break;
            }
        }
    }

    reverse(path.begin(), path.end());
    for(auto c : path) 
        cout<<c<<" ";
    cout<<endl;
}

