/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* see https://cp-algorithms.com/graph/finding-cycle.html
 * Note that this finds a cylce of size 2. To skip cylces of size 2
 * add a parent parameter to dfs(v,p).
 **/

int n;
vector<vector<int>> adj;
vector<char> color;
vector<int> parent;
int cycle_start, cycle_end;
vector<int> cycle;

bool dfs(int v) {
    color[v] = 1;
    for (int u : adj[v]) {
        if (color[u] == 0) {
            parent[u] = v;
            if (dfs(u))
                return true;
        } else if (color[u] == 1) {
            cycle_end = v;
            cycle_start = u;
            return true;
        }
    }
    color[v] = 2;
    return false;
}

/* set graph in adj[][], result cycle found in cycle[] */
void find_cycle() {
    color.assign(n, 0);
    parent.assign(n, -1);
    cycle_start = -1;

    /* MOD START:
     * In this modification we find all roots of the graph,
     * and then start search from the roots first.
     */
    vb root(n, true);
    for(int i=0; i<n; i++) {
        for(int v : adj[i])
            root[v]=false;
    }

    for(int v=0; v<n; v++) {
        if(root[v] && color[v]==0 && dfs(v))
            break;
    }

    if(cycle_start==-1) {
    /*  MOD END */
        for (int v = 0; v < n; v++) {
            if (color[v] == 0 && dfs(v))
                break;
        }
    }

    if (cycle_start != -1) {
        cycle.push_back(cycle_start);
        for (int v = cycle_end; v != cycle_start; v = parent[v])
            cycle.push_back(v);
        cycle.push_back(cycle_start);
        reverse(cycle.begin(), cycle.end());
    }
}

void solve() {
    cin>>n;
    cini(m);
    adj.resize(n);
    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        adj[u].push_back(v);
    }

    find_cycle();

    if(cycle.size()==0) {
        cout<<"IMPOSSIBLE"<<endl;
    } else {
        cout<<cycle.size()<<endl;
        for(int i : cycle)
            cout<<i+1<<" ";
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
