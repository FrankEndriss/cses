#include <bits/stdc++.h>
using namespace std;

int n, dp[1000001];
int main() {
    cin>>n;

    for(int i=1; i<min(n+1, 10); i++)
        dp[i]=1;

    for(int i=10; i<=n; i++) {
        int c=1e9, j=i, d;
        do {
            d=j%10;
            if(d>0)
                c=min(c, dp[i-d]+1);
            j/=10;
        } while(j>0);
        dp[i]=c;
    }
    cout<<dp[n]<<endl;
}

