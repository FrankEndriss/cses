
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Brute force would be to:
 * Build any possible subgroup of all developers with p<=x.
 * Add number of remaining possibilities (remaining elements with max(x)=x-p)
 *
 * Since there are like (n-1)! such subgroups this is not possible.
 *
 * ***
 * What about iterating x?
 * For x=0 we find ans by:
 * let f[j] be the freq of element with value j, then
 * let sg(x)=number of possible subgroups of f[j] elements: x! ??? or some stars'n'bars
 * foreach j
 *    ans+=sg(f[j])
 *
 * Now, if x increases by 1, we can combine one pair of groups of adjacent members:
 * Consider f[4],f[5], then we can build 
 * sg(f[4])*sg(f[5]) new groups of penalty 1.
 * ...and then?
 **/
const int MOD=1e9+7;
void solve() {
    cini(n);
    cini(x);
    cinai(t,n);
    sort(all(t));

    /* dp[i][j]=number of ways if we start at idx=i with max penalty p */
    vvi dp(n, vi(x+1, -1));

    function<int(int,int)> go=[&](int i, int p) {
        if(i==n)
            return 1LL;

        if(dp[i][p]>=0)
            return dp[i][p];


        int ans=0;
        for(int j=i+1; j<=n && t[j-1]-t[i]<=p; j++) {
            ans+=go(j, p-(t[j-1]-t[i]));
            ans%=MOD;
        }

        return dp[i][p]=ans;
    };

    int ans=go(0, x);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
