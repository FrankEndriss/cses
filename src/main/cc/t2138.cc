
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

constexpr int N=5e4+3;
vb vis(N);
vector<bitset<N>> dp(N);
vvi adj(N);

void solve() {
    cini(n);
    cini(m);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
    }


    function<void(int)> dfs=[&](int v) {
        vis[v]=true;

        dp[v][v]=1;
        for(int chl : adj[v]) {
            if(!vis[chl])
                dfs(chl);

            dp[v]|=dp[chl];
        }
    };

    for(int i=0; i<n; i++) {
        if(!vis[i])
            dfs(i);
        cout<<dp[i].count()<<" ";
    }
    cout<<endl;
}

signed main() {
    solve();
}
