/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n, x;
    cin>>n>>x;
    vector<pair<int, int>> a(n);
    fori(n) {
        cin>>a[i].first;
        a[i].second=i+1;
    }
    sort(a.begin(), a.end());

    auto find2=[&](int i1, int i2) {
        ll sum=a[i1].first+a[i2].first;
        ll search=x-sum;
        int l=i1+1;
        if(l==i2)
            l++;
        int r=n-1;
        while(l<r-1) {
            ll sum2=a[l].first+a[r].first;
            if(sum2==search ) {
                return make_pair(l, r);
            } else if(sum2>search) {
                r--;
                if(r==i2)
                    r--;
            } else {    
                l++;
                if(l==i2)
                    l++;
            }
        }
        return make_pair(-1, -1);
    };

    for(int i=0; i<n-2; i++) {
        for(int j=i+1; j<n-1; j++) { 
            pair<int, int> pii=find2(i, j);
            if(pii.first>0) {
                cout<<a[i].second<<" "<<a[j].second<<" "<<a[pii.first].second<<" "<<a[pii.second].second<<endl;
                return 0;
            }
        }
    }

    cout<<"IMPOSSIBLE"<<endl;
}

