/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const vi dx= { -2, -2,  2, 2, -1, -1,  1, 1 };
const vi dy= { -1,  1, -1, 1, -2,  2, -2, 2 };
/* Create Knight's tour by warnsdorf's rule.
 * Move the knight to where it has the least possible next moves.
 *
 * Since there are cases where it does not work, we need to 
 * flip the board once starting cell is not in upper half.
 */
void solve() {
    vvi m(8, vi(8));
    cini(y);
    cini(x);
    x--;
    y--;

    bool flip=false;
    if(y>3) {
        y=7-y;
        flip=true;
    }

    m[x][y]=1;

    //cerr<<"x="<<x<<" y="<<y<<endl;

    function<bool(int,int)> ok=[&](int xx, int yy) {
        return xx>=0 && xx<8 && yy>=0 && yy<8;
    };

    function<vector<pii>(int,int)> movs=[&](int xx, int yy) {
        vector<pii> ans;
        for(int d=0; d<8; d++) {
            pii p= {xx+dx[d], yy+dy[d]};
            if(ok(p.first,p.second) && m[p.first][p.second]==0)
                ans.push_back(p);
        }
        return ans;
    };

    int cnt=1;
    while(cnt<64) {
        vector<pii> pn=movs(x,y);
        assert(pn.size()>0);
        pii ma=*min_element(all(pn), [&](pii p1, pii p2) {
            int cnt1=movs(p1.first, p1.second).size();
            int cnt2=movs(p2.first, p2.second).size();
            if(cnt1==cnt2)
                return p1>p2;
            else
                return cnt1<cnt2;
        });
        x=ma.first;
        y=ma.second;
        cnt++;
        m[x][y]=cnt;
    }

    if(flip) {
        for(int i=0; i<8; i++)
            reverse(all(m[i]));
    }

    for(int i=0; i<8; i++) {
        for(int j=0; j<8; j++) 
            cout<<m[i][j]<<" ";
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
