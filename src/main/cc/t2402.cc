
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Looks trivial, isn't it?
 * We must not put a number on a stack if a smaller number
 * is on that stack. Because we never can pop such two numbers.
 *
 * So, we put all numbers on the stack with the smaller smallest number.
 * Because that gives better result as putting it on the stack with
 * the bigger smallest number.
 * After each push we pop all numbers possible.
 *
 * To allways know the smallest number on the stack we maintain
 * per stack a set of the values on that stack.
 */
const int INF=1e9;
void solve() {
    cini(n);
    vector<set<int>> se(2);
    vector<stack<int>> st(2);

    se[0].insert(INF);
    se[1].insert(INF);
    st[0].push(INF);
    st[1].push(INF);

    vi ans(n);
    int todo=1; /* next number we want to pop */
    for(int i=0; i<n; i++) {
        cini(aux);

        int mi0=*se[0].begin();
        int mi1=*se[1].begin();

        cerr<<"aux="<<aux<<" mi0="<<mi0<<" mi1="<<mi1<<endl;

        int ins=-1;
        if(aux<mi0) { 
            if(aux<mi1) {
                if(mi0<=mi1) {
                    ins=0;
                } else {
                    ins=1;
                }
            } else
                ins=0;
        } else if(aux<mi1) {
            ins=1;
        } else {
            cout<<"IMPOSSIBLE"<<endl;
            return;
        }

        se[ins].insert(aux);
        st[ins].push(aux);
        ans[i]=ins+1;

        while(todo<=n) {
            if(st[0].top()==todo) {
                st[0].pop();
                se[0].erase(se[0].begin());
                todo++;
            } else if(st[1].top()==todo) {
                st[1].pop();
                se[1].erase(se[1].begin());
                todo++;
            } else
                break;
        }
    }
    assert(st[0].size()==1);
    assert(st[1].size()==1);

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
