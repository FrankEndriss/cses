/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


/** stolen from tourist, Problem D, https://codeforces.com/contest/1208/standings/page/1 */
template <typename T>
struct fenwick {
    vector<T> fenw;
    int n;

    fenwick(int _n) : n(_n) {
        fenw.resize(n);
    }

    void update(int x, T v) {
        while (x < n) {
            fenw[x] += v;
            x |= (x + 1);
        }
    }

    /* get sum of range (0,x), including x */
    T query(int x) {
        T v{};
        while (x >= 0) {
            v += fenw[x];
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

template <typename T>
struct fenwick2d {
    vector<fenwick<T>> fenw2d;
    int n, m;

    fenwick2d(int x, int y) : n(x), m(y) {
        fenw2d.resize(n, fenwick<int>(m));
    }

    void update(int x, int y, T v) {
        while (x < n) {
            fenw2d[x].update(y, v);
            x |= (x + 1);
        }
    }

    T query(int x, int y) {   // range 0..x/y, including x/y
        x=min(x, n-1);
        y=min(y, m-1);
        T v{};
        while (x >= 0) {
            v += fenw2d[x].query(y);
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

/* Well, this is the standard problem for a 2D fenwick tree.
 * But since there are no updates we can use more efficient
 * prefix sums 2D array.
 */
void solve() {
    cini(n);
    cini(q);

    fenwick2d<int> fen(n, n);

    for(int i=0; i<n; i++) {
        cins(s);
        for(size_t j=0; j<s.size(); j++) {
            if(s[j]=='*')
                fen.update(i,j,1);
        }
    }

    for(int _=0; _<q; _++) {
        cini(t);
        if(t==1) {
            cini(i);i--;
            cini(j);j--;
            int val=fen.query(i, j);
            val-=fen.query(i-1, j);
            val-=fen.query(i, j-1);
            val+=fen.query(i-1, j-1);
            if(val)
                fen.update(i,j,-1);
            else
                fen.update(i,j,1);

        } else {
            cini(i1);
            i1--;
            cini(j1);
            j1--;
            cini(i2);
            i2--;
            cini(j2);
            j2--;

            int ans=fen.query(i2, j2);
            ans-=fen.query(i1-1, j2);
            ans-=fen.query(i2, j1-1);
            ans+=fen.query(i1-1, j1-1);

            cout<<ans<<endl;
        }
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
