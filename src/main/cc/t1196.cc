

/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Unclear how to solve.
 * Maybe we have to implement Eppstein, 
 * see https://en.wikipedia.org/wiki/K_shortest_path_routing
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vector<vector<pii>> adj(n);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        cini(c);
        adj[a].emplace_back(b,c);
    }
    //cerr<<"after parse"<<endl;

    priority_queue<pii> q;
    q.emplace(0,0);
    while(k>0 && q.size()) {
        auto [c,v]=q.top();
        q.pop();
        if(v==n-1) {
            cout<<-c<<" ";
            k--;
        }
        //cerr<<"c="<<c<<" v="<<v<<endl;

        for(auto chl : adj[v])
            q.emplace(c-chl.second,chl.first);

        /*
        while(q.size()>k) {
            auto it2=q.end();
            it2--;
            q.erase(it2);
        }
        */
    }

    cout<<endl;
}

signed main() {
        solve();
}
