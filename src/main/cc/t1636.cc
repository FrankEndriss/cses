/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const int MOD=1e9+7;

int main() {
    int n, x;
    cin>>n>>x;

    vector<int> dp(x+1);
    dp[0]=1;

    for(int i=0; i<n; i++) {
        int a;
        cin>>a;
        for(int j=0; j+a<=x; j++) {
            dp[j+a]+=dp[j];
            if(dp[j+a]>=MOD)
                dp[j+a]-=MOD;
        }
    }
    cout<<dp[x]<<endl;
}

