/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, x;
    cin>>n>>x;
    vector<int> p(n);
    fori(n)
        cin>>p[i];

    sort(p.begin(), p.end());

    int l=0;
    int r=n-1;
    int ans=0;
    while(l<=r) {
        if(p[l]+p[r]<=x)
            l++;

        ans++;
        r--;
    }

    cout<<ans<<endl;
}

