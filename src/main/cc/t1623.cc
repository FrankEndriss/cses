/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n;
    cin>>n;
    vector<ll> p(n);
    ll xx=0;
    fori(n) {
        cin>>p[i];
        xx+=p[i];
    }
    ll x=(xx+1)/2;

    /* find max sum(p[i]) lt or eq to x */

    set<ll> dp;
    dp.insert(0);
    ll mx=0;
    for(int i=0; i<n; i++) {
        set<ll> dp2;
        for(auto k : dp) {
            dp2.insert(k);
            if(k+p[i] <=x ) {
                mx=max(mx, k+p[i]);
                dp2.insert(k+p[i]);
            }
        }
        swap(dp, dp2);
    }

    cout<<abs((xx-mx)-mx)<<endl;
}

