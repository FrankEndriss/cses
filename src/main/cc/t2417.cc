
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;


/* filter windows lineends */
void mygetline(string &s) {
    s.clear();
    int c=getchar_unlocked();
    while(c!='\n' && c>=0) {
        if(c!=13)
            s.push_back((char)c);
        c=getchar_unlocked();
    }
}

inline void fs(int &number) {
#define gc() getchar_unlocked()

    int c = gc();
    while (c == ' ' || c == 10)
        c = gc();

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }
}

constexpr int N=1e6+1;

/*
 * It is very complecated.
 * A "pair" in the problem is a distinct pair
 * of indexes, so if numbers occur multiple times
 * they count multiple times.
 *
 * Consider a[i]==1 as edgecase. All pairs with a 1 in
 * it contribute.
 *
 * Consider a[] sorted non descending.
 * Then, foreach a[i] that a[i] can form at most
 * n-1-i pairs.
 * Find the numbers of pairs with gcd!=1. Do this by
 * iterating the primefactors of a[i] starting at a[i].
 * Foreach number hit subtract the freq of that number
 * once from the number of pairs.
 * Finally add the number of the remaining pairs to ans.
 *
 * So, this seems to work fine, but TLE.
 * Optimize by
 * -reuse previous query if a[i-1]==a[i]
 * -reuse previous prime factorization if a[i-1]==a[i]
 * -do a faster primefactorization for all numbers up to N at once
 * -precalc the primes up to ~1000... how?
 ****
 * Per number a[i] we have a set of its primefactors, and
 * we need to find the count of other numbers
 * having a disjoint set of primefactors, ie none in common.
 *
 * With inclusion/exclusion we could do something like:
 * -add the count of all numbers
 * -remove the sum/count of numbers having any one of the primefactors
 *  in common.
 * -add the count of numbers having 2 of the primefactors in common
 * -remove the count of numbers having 3 of the primefactors in common
 * ...
 * So, we need to loop foreach number over its primefactors, let p=pcnt(i)
 * be the number of primefactors of a[i], then
 * sum(i=1..p, nCr(i,p)
 *
 * Note that we need to consider each primefactor only once, in example
 * 2*3==6 has exactly the same number of pairs as 2*2*3=12
 * This means that no number has more than 7 distinct primefactors, since
 * 2*3*5*7*11*13*17==510510
 */
void solve() {
    int n;
    fs(n);
    vi a(n);
    vi x(N);
    for(int i=0; i<n; i++) {
        fs(a[i]);
        x[a[i]]++;
    }

    vi cntp(N);    /* cntp[p]=in how much numbers have divisor p */
    vvi fac(N);    /* fac[i]= distinct primefactors of i */
    vb f(N);
    for(int i=2; i<N; i++) {
        bool ispr=!f[i];
        for(int j=i; j<N; j+=i) {
            if(x[j]) {
                if(ispr)
                    fac[j].push_back(i);
                cntp[i]+=x[j];
            }
            f[j]=true;
        }
    }

    sort(all(a));
    ll ans=0;
    int pcnt;

    for(int i=0; i<n; i++) {
        if(a[i]==1) {
            ans+=n-1;
            continue;
        }

        if(i>0 && a[i]==a[i-1]) {
            ans+=pcnt;
            continue;
        }

        int cnt=n-1;    /* pairs with all other numbers */
        const int aa=a[i];
        const int nn=fac[aa].size();
        assert(nn>0);

        function<void(int,int,int)> go=[&](int val, int bcnt, int idx) {
            if(idx==nn) {
                if(bcnt&1) 
                    cnt-=(cntp[val]-1);
                else
                    cnt+=(cntp[val]-1);
                return;
            }

            go(val, bcnt, idx+1);
            go(val*fac[aa][idx], bcnt+1, idx+1);
        };

        for(int j=0; j<nn; j++)
            go(fac[aa][j], 1, j+1);

        ans+=cnt;
        pcnt=cnt;
    }

    cout<<ans/2<<endl;
}

signed main() {
    solve();
}
